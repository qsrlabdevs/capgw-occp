CAPGW 

1.6:
   - Issue:
    - TAS component open thousands connections to redis server thus redis server cannot accept connections from any other client
        - Solution:
            - Use nexus-middleware-api for redis connection management, same code is used by all NEXUS components
