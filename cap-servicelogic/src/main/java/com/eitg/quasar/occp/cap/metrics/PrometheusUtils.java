package com.eitg.quasar.occp.cap.metrics;

import io.prometheus.client.dropwizard.samplebuilder.CustomMappingSampleBuilder;
import io.prometheus.client.dropwizard.samplebuilder.MapperConfig;
import io.prometheus.client.dropwizard.samplebuilder.SampleBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PrometheusUtils {
    public static SampleBuilder countersBuilder(){
        MapperConfig tasPing = new MapperConfig();
        tasPing.setMatch("tas.appserver.*");
        tasPing.setName("appServer");
        {
            Map<String, String> labels = new HashMap<>();
            tasPing.setLabels(labels);
            labels.put("name","${0}");
        }



        MapperConfig callStart = new MapperConfig();
        callStart.setMatch("tas.cap.callStart.*");
        callStart.setName("callStart");
        {
            Map<String, String> labels = new HashMap<>();
            callStart.setLabels(labels);
            labels.put("cause","${0}");
        }

        MapperConfig callStartVssf = new MapperConfig();
        callStartVssf.setMatch("tas.cap.vssf.callStart.*");
        callStartVssf.setName("vssfCallStart");
        {
            Map<String, String> labels = new HashMap<>();
            callStartVssf.setLabels(labels);
            labels.put("cause","${0}");
        }


        MapperConfig callPoll = new MapperConfig();
        callPoll.setMatch("tas.cap.callPoll.*");
        callPoll.setName("callPoll");
        {
            Map<String, String> labels = new HashMap<>();
            callPoll.setLabels(labels);
            labels.put("cause","${0}");
        }

        MapperConfig callPollVssf = new MapperConfig();
        callPollVssf.setMatch("tas.cap.vssf.callPoll.*");
        callPollVssf.setName("vssfCallPoll");
        {
            Map<String, String> labels = new HashMap<>();
            callPollVssf.setLabels(labels);
            labels.put("cause","${0}");
        }



        MapperConfig callAnswered = new MapperConfig();
        {
            callAnswered.setMatch("tas.cap.callAnswered.*");
            callAnswered.setName("callAnswered");
            Map<String, String> labels = new HashMap<>();
            callAnswered.setLabels(labels);
            labels.put("cause","${0}");
        }

        MapperConfig callAnsweredVssf = new MapperConfig();
        {
            callAnsweredVssf.setMatch("tas.cap.vssf.callAnswered.*");
            callAnsweredVssf.setName("vssfCallAnswered");
            Map<String, String> labels = new HashMap<>();
            callAnsweredVssf.setLabels(labels);
            labels.put("cause","${0}");
        }


        MapperConfig callEarlyAnswered = new MapperConfig();
        callEarlyAnswered.setMatch("tas.cap.callEarlyAnswered");
        callEarlyAnswered.setName("callEarlyAnswered");

        MapperConfig callEarlyAnsweredVssf = new MapperConfig();
        callEarlyAnsweredVssf.setMatch("tas.cap.vssf.callEarlyAnswered");
        callEarlyAnsweredVssf.setName("vssfCallEarlyAnswered");


        MapperConfig callEnd = new MapperConfig();
        callEnd.setMatch("tas.cap.callEnd.*");
        callEnd.setName("callEnd");
        {
            Map<String, String> labels = new HashMap<>();
            callEnd.setLabels(labels);
            labels.put("cause", "${0}");
        }

        MapperConfig callEndVssf = new MapperConfig();
        callEndVssf.setMatch("tas.cap.vssf.callEnd.*");
        callEndVssf.setName("vssfCallEnd");
        {
            Map<String, String> labels = new HashMap<>();
            callEndVssf.setLabels(labels);
            labels.put("cause", "${0}");
        }

        MapperConfig configTimeRes = new MapperConfig();
        configTimeRes.setMatch("tas.cap.time.*");
        configTimeRes.setName("response_time");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            configTimeRes.setLabels(lab);
        }

        MapperConfig tasResponse = new MapperConfig();
        tasResponse.setMatch("tas.cap.resp.*");
        tasResponse.setName("response");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasResponse.setLabels(lab);
        }

        MapperConfig tasOverload = new MapperConfig();
        tasOverload.setMatch("tas.overload.*");
        tasOverload.setName("overload");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasOverload.setLabels(lab);
        }

        MapperConfig tasSessions = new MapperConfig();
        tasSessions.setMatch("tas.sessions.*");
        tasSessions.setName("sessions");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasSessions.setLabels(lab);
        }

        MapperConfig makeCalls = new MapperConfig();
        makeCalls.setMatch("tas.makeCall.*");
        makeCalls.setName("makeCall");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            makeCalls.setLabels(lab);
        }


        return new CustomMappingSampleBuilder(Arrays.asList(tasPing,
                callStart,
                callAnswered,
                callEarlyAnswered,
                callPoll,
                callEnd,
                configTimeRes,
                tasResponse,
                tasOverload,
                tasSessions,
                callPollVssf,
                callAnsweredVssf,
                callEarlyAnsweredVssf,
                callStartVssf,
                callEndVssf,
                makeCalls));
    }
}
