package com.eitg.quasar.occp.cap.metrics;

import com.codahale.metrics.Gauge;

public class ValueGauge implements Gauge<Integer> {
    private Integer value=0;

    public void setValue(Integer value){
        this.value=value;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }
}
