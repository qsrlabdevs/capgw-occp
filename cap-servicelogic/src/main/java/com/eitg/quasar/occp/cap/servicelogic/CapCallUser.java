package com.eitg.quasar.occp.cap.servicelogic;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import com.eitg.quasar.occp.cap.metrics.MetricsServer;
import com.eitg.quasar.occp.overload.OverloadProtection;
import com.eitg.quasar.occp.utils.*;
import com.hp.opencall.camel.camel1.cap_datatypes.LegID;
import com.hp.opencall.camel.camel1.cap_datatypes.LegType;
import com.hp.opencall.camel.camel1.cap_datatypes.ReleaseCallArg;
import com.hp.opencall.camel.camel1.map_commondatatypes.CellIdFixedLength;
import com.hp.opencall.camel.camel1.map_commondatatypes.CellIdOrLAI;
import com.hp.opencall.camel.camel2.cap_datatypes.*;

import com.hp.opencall.camel.camel2.cap_datatypes.EventReportBCSMArg;
import com.hp.opencall.camel.camel2.map_commondatatypes.LAIFixedLength;
import com.hp.opencall.camel.camel3.cap_datatypes.*;
import com.hp.opencall.camel.camel3.cap_datatypes.ReceivingSideID;
import com.hp.opencall.camel.camel3.cap_sms_ops_args.ReleaseSMSArg;
import com.hp.opencall.camel.camel3.map_commondatatypes.AgeOfLocationInformation;
import com.hp.opencall.camel.camel3.map_commondatatypes.CellGlobalIdOrServiceAreaIdFixedLength;
import com.hp.opencall.camel.camel3.map_commondatatypes.CellGlobalIdOrServiceAreaIdOrLAI;
import com.hp.opencall.camel.camel3.oc_camel3_pdus.*;
import com.hp.opencall.camel.camel3.oc_camel3_pdus.CAMEL_FCIBillingChargingCharacteristics;
import com.hp.opencall.camel.camel3.oc_camel3_pdus.ConnectArg;

import com.hp.opencall.camel.camel3.oc_camel3_pdus.FurnishChargingInformationArg;
import com.hp.opencall.camel.camel3.oc_camel3_pdus.InitialDPArg;
import com.hp.opencall.camel.camel3.oc_camel3_pdus.PlayAnnouncementArg;
import com.hp.opencall.camel.camel3.oc_camel3_pdus.PromptAndCollectUserInformationArg;
import com.hp.opencall.camel.common.cs1_datatypes.RedirectionInformation;
import com.hp.opencall.camel.common3_4.cs2_datatypes.MiscCallInfo;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.cs.events.*;
import com.hp.opencall.ngin.scif.cs.events.sms.*;
import com.hp.opencall.ngin.scif.events.InPollEvent;
import com.hp.opencall.ngin.scif.extensions.vendors.*;
import com.hp.opencall.ngin.scif.parameters.*;
import com.hp.opencall.ngin.scif.resources.IvrCallLeg;
import com.oss.asn1.ASN1Project;
import com.oss.asn1.BERCoder;
import com.oss.asn1.Null;
import com.oss.asn1.OctetString;
import com.oss.storage.OSSByteStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.businesslogic.Parameters;
import com.eitg.quasar.occp.cap.utils.TelecomUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.imscapi.imslet.ApplicationSession;

import javax.xml.bind.DatatypeConverter;


public class CapCallUser implements CallUser, MessageInterceptor {

	private String asn1TimeAndTimezone=null;
	private Map<String,Object> asn1LocationInformation=null;
	private Integer asn1ErbLegId=null;
	private Map<String,Object> asn1Erb = null;
	private Map<String,Object> asn1Connect = null;
	private Map<String,Object> asn1Release = null;
	public void setAsn1Release(Map<String,Object> value){
		this.asn1Release = asn1Release;
	}
	public void setAsn1Connect(Map<String,Object> value){
		_log.debug("set asn1 connect:{}",value);
		this.asn1Connect=value;
	}
	public void setAsn1Erb(Map<String,Object> value){
		asn1Erb=value;
		_log.debug("set asn1 erb:{}",asn1Erb);
	}
	private String smsDataCoding=null;
	private String smsShortMsgSecInfo=null;
	private String smsProtocolIdentif=null;
	private String smsValidityPeriod=null;
	private boolean overload=false;

	private boolean vssfCallUser = false;
	private boolean callEndDone = false;
	public static ConcurrentHashMap<String, CapCallUser> calls = new ConcurrentHashMap<>();
	
	private static final Logger _log = LoggerFactory.getLogger(CapCallUser.class.getName());

	private JsonSession jsonSession=null;
	public JsonSession getJsonSession() {
		return this.jsonSession;
	}
	private JsonSCIFRequest jsonCall=null;

	private Map<String,InPollEvent> vssfPendingEvents = new HashMap<>();
	public void setAsn1ErbLegId(Integer value){
		this.asn1ErbLegId=value;
	}


	public static final Integer RouteSelectFailure = 4;
	public static final Integer OCalledPartyBusy = 5;
	public static final Integer ONoAnswer = 6;
	public static final Integer OAnswer = 7;
	public static final Integer OMidCall = 8;
	public static final Integer ODisconnect = 9;
	public static final Integer OAbandon = 10;
	public static final Integer TTerminationAttemptAuthorized = 12;
	public static final Integer TCalledPartyBusy = 13;
	public static final Integer TNoAnswer = 14;
	public static final Integer TAnswer = 15;
	public static final Integer TMidCall = 16;
	public static final Integer TDisconnect = 17;
	public static final Integer TAbandon = 18;

	/**
	 * The call object and the application session for this callUser
	 */
	private Call call;
	private ApplicationSession appSession;
	private long timeDiff = 0;
//	private RawMediaCallLeg rawMediaLeg =null;
	String egressQueue =null;
	private boolean fireAndFortget=false;

	private Map<String,Object> asn1Param = new HashMap<>();
	//vssf params
	private List<InCallMonitorEvent> vssfMonitorEvents = new ArrayList<>();
	private List<InSmsMonitorEvent> vssfSMSMonitorEvents = new ArrayList<>();


	private String lockEventId=null;
	private long lockEventIdTime=0;

	private String session=null;
	private String callId=null;
	private InCallChargingEvent vssfChargingEvent=null;
	private InCallDirectMediaEvent vssfDirectMediaEvent=null;
	private InCallDiscMediaEvent vssfdiscMediaEvent=null;
	private InCallMediaPAEvent vssfMediaPAEvent=null;
	private InCallMediaPACEvent vssfMediaPACEvent=null;
	private CallLeg srfLeg = null;
	private long lastQueueSetTime=0;
	private Collection<InPollEvent> lastINPollEventList;
	private boolean stateInitial=false;

	public void firstResponseReceived(){
		_log.debug("First response received");
		this.stateInitial=false;
	}

	public boolean isStateInitial(){
		return this.stateInitial;
	}

	public void setSession(String value){
		this.session=value;
	}

	public void setLockEventId(String value){
		this.lockEventId=value;
		this.lockEventIdTime= Instant.now().toEpochMilli();
		_log.debug("Set event-id:{}",this.lockEventId);
	}

	public CapCallUser(){

		this.call=null;
		this.appSession=null;
		this.enableVssfSkip=true;
	}

	public void setVssfCallUser() {
		this.vssfCallUser=true;
	}
	private boolean answered=false;
	private List<Map<String,Object>> pendingVssfPollEvents= null;

	private static Map<Integer,Integer> skipEvents = createSkipEventsMap();

	private static Map<Integer,Integer> createSkipEventsMap(){
		Map<Integer,Integer> val = new HashMap<>();
		val.put(4,4);
		val.put(17,17);
		return val;
	}

	private boolean enableVssfSkip=false;

	public CapCallUser(Call aCall, ApplicationSession anAppSession) {
		_log.debug("Creating CapCalluser call:{} session:{}",aCall,anAppSession);
		// get current UTC time in milliseconds since epoch
		long currentTimeMs = Calendar.getInstance().getTimeInMillis();

		//Call jsonCall = new JsonCall(aCall);
		//aCall = JsonCall.swap(jsonCall, (jsonCall=aCall));
		
		// Store the call and the application session in class variables.
		call = aCall;
		appSession = anAppSession;
		appSession.setAttribute("scifCallUser", this);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(anAppSession.getCreationTime());
		timeDiff = currentTimeMs - anAppSession.getCreationTime();
		_log.info("CapCallUser constructor, creation time:{}, " , anAppSession.getCreationTime() );

		jsonSession = new JsonSession(appSession);
		jsonCall = new JsonSCIFRequest(call);
		
	
		//egressQueue = "app_"+jsonCall.getCallId();
		callId=jsonCall.getCallId();
		calls.put(callId, this);

		_log.debug("Switching point:{}",call.getParameterSet(CommonParameterSet.class).getSwitchingPoint());


		//set message interceptor
		CommonParameterSet commonParameterSet=call.getParameterSet(CommonParameterSet.class);
		commonParameterSet.setMessageInterceptor(this);

		this.egressQueue=Parameters.get().getQueueEgress();
	}

	// ************************ callStart ************************************
	public void callStart(Cause aCause, CallLeg aFirstLeg, Contact aCalledParty) {
		// Log the call Id and the cause for reaching Start state.
		_log.debug("callStart NEW invoked with cause:{}, callleg:{}, contact:{} ", aCause  , aFirstLeg , aCalledParty);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLSTART);
		if( jsonCall!=null) {
			eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		}
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.CALL_USER, this);

		if( this.vssfCallUser==true ){
			String counter = "tas.cap.vssf.callStart." + aCause;
			MetricsServer.get().counterIncrement(counter);

		} else {
			String counter = "tas.cap.callStart." + aCause;
			MetricsServer.get().counterIncrement(counter);
		}
		
		
		Long lstartTime = Calendar.getInstance().getTimeInMillis();
		try{

			if( aCause==Cause.NONE ){
				this.stateInitial=true;
				//this is initial event, check overload
				if( OverloadProtection.inOverload || OverloadProtection.inDegraded ){
					//reject event
					String overloadCounter = "tas.overload.initial";
					MetricsServer.get().counterIncrement(overloadCounter);
					Map<String,Object> rawIncoming = jsonCall.getJsonCallStart(aCause, aFirstLeg, aCalledParty, this.vssfCallUser);
//					_log.error("Overload reject call session:{} event:{}",jsonCall.getCallId(),rawIncoming);
//					call.abortCall(Cause.REJECTED);
					_log.error("Overload connect call session:{} event:{}",jsonCall.getCallId(),rawIncoming);
					call.leaveCall(Cause.DONE, aCalledParty);
					this.overload=true;
					return;
				}
			}
			//prepare JSON message
			Map<String,Object> rawMessage = jsonCall.getJsonCallStart(aCause, aFirstLeg, aCalledParty, this.vssfCallUser);

			if( asn1Param!=null && asn1Param.size()>0) {
				rawMessage.put("asn1", asn1Param);
			}
			
			ObjectMapper mapper = new ObjectMapper();
			String json=null;
			json = mapper.writeValueAsString(rawMessage);
			
			_log.debug("JSON:{}",json);
			
			if( json!=null){
				//send this to redis queue
				_log.debug("Sending JSON message to redis ...");
				RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
				if( this.lockEventId!=null) {
					rawMessage.put("event-id",this.lockEventId);
					_log.error("Execution time vssf session:{} :{}",this.getCallId(),Instant.now().toEpochMilli()-this.lockEventIdTime);
					this.lockEventId=null;
					this.lockEventIdTime=0;
				}
				//check whether session is set
				if( session!=null ){
					rawMessage.put("session",session);
				}

				if( this.vssfCallUser==true ){
					rawMessage.put("vssf",true);
				}


				getValidQueue();
				boolean status = RedisPool.get().sendToQueue(rawMessage, this.egressQueue);
				asn1Param.clear();
			}
		} catch (Exception e){
			_log.error("Exception:{}",e.getMessage());
		} finally {
			_log.info("Execution time, cause:{}, spent time:{}",aCause, (Calendar.getInstance().getTimeInMillis()-lstartTime));
			jsonCall.incrementEventId();
		}
	}

	// This service does not rely on an early media feature
	public void callEarlyAnswered(CallLeg aNewLeg) {
		if( this.vssfCallUser==true ){
			String counter = "tas.cap.vssf.callEarlyAnswered";
			MetricsServer.get().counterIncrement(counter);
		} else {
			String counter = "tas.cap.callEarlyAnswered";
			MetricsServer.get().counterIncrement(counter);

		}

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLEARLYANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		eventData.put(JsonSCIFResponse.CALL_USER, this);

		this.srfLeg=aNewLeg;
		try{
			// _log.info("Call early-answered");
			Map<String, Object> rawEarlyAnswered = jsonCall.getEarlyMediaAnswered(aNewLeg, this.vssfCallUser);
			_log.info(" rawEarlyAnswered:{}",rawEarlyAnswered);


            if (aNewLeg instanceof IvrCallLeg){
                IvrCallLeg ivrCallLeg = (IvrCallLeg) aNewLeg;
                _log.debug("MRF leg, set media user:{}",ivrCallLeg.toString());
                ivrCallLeg.setIvrUser(new CapIvrUser(jsonCall, jsonSession, ivrCallLeg, this.egressQueue));
            }

			if( asn1Param!=null && asn1Param.size()>0) {
				rawEarlyAnswered.put("asn1", asn1Param);
			}
			

			RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
			if( this.lockEventId!=null) {
				rawEarlyAnswered.put("event-id",this.lockEventId);
				_log.error("Execution time vssf session:{} :{}",this.getCallId(),Instant.now().toEpochMilli()-this.lockEventIdTime);
				this.lockEventId=null;
				this.lockEventIdTime=0;
			}

			if( session!=null ){
				rawEarlyAnswered.put("session",session);
			}

			if( this.vssfCallUser==true ){
				rawEarlyAnswered.put("vssf",true);
			}

			getValidQueue();
			boolean status =  RedisPool.get().sendToQueue(rawEarlyAnswered, this.egressQueue);
			asn1Param.clear();

		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callAnswered(Cause aCause, CallLeg aNewLeg) {
		if( this.vssfCallUser==true ){
			String counter = "tas.cap.vssf.callAnswered." + aCause;
			MetricsServer.get().counterIncrement(counter);
		} else {
			String counter = "tas.cap.callAnswered." + aCause;
			MetricsServer.get().counterIncrement(counter);
		}

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		eventData.put(JsonSCIFResponse.CALL_USER, this);

		this.srfLeg = aNewLeg;
		
		try{
			_log.info("callAnswered cause:{}, leg:{}",aCause, aNewLeg);
			Map<String, Object> rawAnswered = jsonCall.getAnswered(aCause, aNewLeg, this.vssfCallUser);
			_log.info(" rawAnswered:{}",rawAnswered);
			if (aNewLeg instanceof IvrCallLeg){
				IvrCallLeg ivrCallLeg = (IvrCallLeg) aNewLeg;
				_log.debug("MRF leg, set media user:{}",ivrCallLeg.toString());
				ivrCallLeg.setIvrUser(new CapIvrUser(jsonCall, jsonSession, ivrCallLeg, this.egressQueue));
			}

			if( asn1Param!=null && asn1Param.size()>0) {
				rawAnswered.put("asn1", asn1Param);
			}
			
			RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
			if( this.lockEventId!=null) {
				rawAnswered.put("event-id",this.lockEventId);
				_log.error("Execution time vssf session:{} :{}",this.getCallId(),Instant.now().toEpochMilli()-this.lockEventIdTime);
				this.lockEventId=null;
				this.lockEventIdTime=0;
			}

			if( session!=null ){
				rawAnswered.put("session",session);
			}

			if( this.vssfCallUser==true ){
				rawAnswered.put("vssf",true);
			}

			getValidQueue();
			boolean status =  RedisPool.get().sendToQueue(rawAnswered, this.egressQueue);
			asn1Param.clear();

		} finally {
			jsonCall.incrementEventId();
			this.answered=true;
		}
	}

	public void sendCallPollEvent(Map<String,Object> pollEvent){
		String counter = "tas.cap.callPoll.custom";
		MetricsServer.get().counterIncrement(counter);
		try {
			Map<String, Object> rawPollMessage = jsonCall.getJsonPollEvent(pollEvent, this.vssfCallUser);
			_log.info("Custom call poll raw message:{}", rawPollMessage);

			getValidQueue();
			boolean status =  RedisPool.get().sendToQueue(rawPollMessage, this.egressQueue);
		} catch (Exception e){
			_log.error("Exception",e);
		}
	}

	public void callPoll(ScifEvent anEvent) {

		Map<String, Object> eventData = new HashMap<>();
		String eventNameSuffix="";
		if( this.vssfCallUser==true ){
			String counter = "tas.cap.vssf.callPoll."+ anEvent.getClass().getSimpleName();
			MetricsServer.get().counterIncrement(counter);

		} else {
			String counter = "tas.cap.callPoll."+ anEvent.getClass().getSimpleName();
			MetricsServer.get().counterIncrement(counter);

		}
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLPOLL);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.POLLEVENT, anEvent);
		eventData.put(JsonSCIFResponse.CALL_USER, this);


		try{
			_log.info("callPoll event:{}",eventData);

			if( anEvent instanceof InPollEvent){
				InPollEvent eventPoll = (InPollEvent) anEvent;
				try {
					Collection<InPollEvent> inPollEventList = eventPoll.getInPollEventsList();
					boolean monitorEvent=false;

					for(InPollEvent event : inPollEventList) {
						if (event.getType() == InCallMonitorEvent.IN_CALL_MONITOR_POLL_EVENT_TYPE) {
							_log.debug("Existing events:{}", vssfMonitorEvents);
							vssfMonitorEvents.clear();
							InCallMonitorEvent monitorPollEvent = (InCallMonitorEvent) event;
							try {
								Collection<InCallMonitorEvent> monitorEvents = monitorPollEvent.getInMonitorPollEventsList();
								for (InCallMonitorEvent monitEvent : monitorEvents) {

									_log.debug("Add event: type:{} leg:{}", monitEvent.getType(), monitEvent.getLegID());
									vssfMonitorEvents.add(monitEvent);
								}
							} catch (Exception e) {
								_log.error("Exception", e);
							}
							monitorEvent=true;
						} else if (event.getType() == InCallContinueEvent.IN_CALL_CONTINUE_POLL_EVENT_TYPE) {
							_log.debug("VSSF received continue, accepting ...");
							InCallContinueEvent cue = (InCallContinueEvent) event;
							cue.accept();
						} else if (event.getType() == InCallConnectEvent.IN_CALL_CONNECT_POLL_EVENT_TYPE ) {
							InCallConnectEvent con = (InCallConnectEvent) event;
							_log.debug("VSSF received connect, accepting ... leg:{}",con.toString());

							con.accept();
						} else if (event.getType() == InCallChargingEvent.IN_CALL_CHARGING_POLL_EVENT_TYPE ) {
							_log.debug("VSSF ach , saving it");
							this.vssfChargingEvent = (InCallChargingEvent) event;
						} else if (event.getType() == InCallDirectMediaEvent.IN_CALL_DIRECT_MEDIA_POLL_EVENT_TYPE ) {
							_log.debug("VSSF direct media , saving it");
							this.vssfDirectMediaEvent = (InCallDirectMediaEvent) event;
						} else if (event.getType() == InCallDiscMediaEvent.IN_CALL_DISC_MEDIA_POLL_EVENT_TYPE ) {
							_log.debug("VSSF disconnect media , saving it");
							this.vssfdiscMediaEvent = (InCallDiscMediaEvent) event;
						} else if (event.getType() == InCallMediaPACEvent.IN_CALL_PAC_MEDIA_POLL_EVENT_TYPE ) {
							this.vssfMediaPACEvent=(InCallMediaPACEvent) event;
						} else if (event.getType() == InCallMediaPAEvent.IN_CALL_PA_MEDIA_POLL_EVENT_TYPE ) {
							this.vssfMediaPAEvent = (InCallMediaPAEvent) event;

						} else if (event.getType() == InSmsContinueEvent.IN_SMS_CONTINUE_POLL_EVENT_TYPE ) {
							InSmsContinueEvent cueSms =  (InSmsContinueEvent) event;
							cueSms.accept();
						} else if (event.getType() == InSmsConnectEvent.IN_SMS_CONNECT_POLL_EVENT_TYPE ) {
							InSmsConnectEvent conSMS = (InSmsConnectEvent) event;

							conSMS.accept();
						} else if (event.getType() == InSmsDeliveredEvent.IN_SMS_T_DELIVERED_POLL_EVENT_TYPE) {
							InSmsDeliveredEvent smsDel = (InSmsDeliveredEvent) event;
							smsDel.accept();
						} else if (event.getType() == InSmsFciEvent.IN_SMS_FCI_POLL_EVENT_TYPE) {
							InSmsFciEvent smsFci = (InSmsFciEvent) event;
							smsFci.accept();
						} else if (event.getType() == InSmsMonitorEvent.IN_SMS_MONITOR_POLL_EVENT_TYPE) {
							_log.debug("Existing events:{}", vssfMonitorEvents);
							vssfMonitorEvents.clear();
							InSmsMonitorEvent monitorPollEvent = (InSmsMonitorEvent) event;
							try {
								Collection<InSmsMonitorEvent> monitorEvents = monitorPollEvent.getInMonitorPollEventsList();
								for (InSmsMonitorEvent monitEvent : monitorEvents) {
									_log.debug("Add event: type:{}", monitEvent.getType() );
									vssfSMSMonitorEvents.add(monitEvent);
								}
							} catch (Exception e) {
								_log.error("Exception", e);
							}
						} else if (event.getType() == InCallActivityTestEvent.IN_CALL_ACTIVITY_TEST_POLL_EVENT_TYPE) {
							_log.debug("Accept activity test event, do not send it to flow");
							InCallActivityTestEvent at = (InCallActivityTestEvent) event;
							at.accept();
							//at.forward();
							return ;
						}

						String eventType=Integer.toString(event.getType());
						this.vssfPendingEvents.put(eventType, event);

						if( inPollEventList.size()==1 &&  monitorEvent==true){
							_log.debug("Skip sending only monitor event");
							return;
						}
						lastINPollEventList = inPollEventList;
					}
				}catch (Exception e){
					_log.error("Exception",e);
				}
			}

			Map<String, Object> rawPollMessage = jsonCall.getJsonPollEvent(anEvent, this.vssfCallUser);
			_log.debug("Poll raw message:{}",rawPollMessage);

			if( session!=null ){
				rawPollMessage.put("session",session);
			}

			if( asn1Param!=null && asn1Param.size()>0) {
				rawPollMessage.put("asn1", asn1Param);
			}

			if( this.vssfCallUser==true ){
				rawPollMessage.put("vssf",true);
			}


			//get callPoll items for vssf
			boolean skip=false;
			List<Map<String,Object>> listPollEvents=null;
			_log.debug("Vssf call user:{} enable Vssf skip:{}",this.vssfCallUser,this.enableVssfSkip);
			if( this.vssfCallUser && this.enableVssfSkip ){
				_log.debug("Checking for skip sending callpoll , skiping events:{}",skipEvents);

				Map<String,Object> callPoll =(Map<String,Object>) ((Map<String,Object>)rawPollMessage.get("event"))
						.get("callPoll");
				listPollEvents = (List<Map<String,Object>>)callPoll.get("vssfPollEvents");

				boolean skipAll=true;
				for(Map<String,Object> item : listPollEvents){
					Integer eventType = (Integer) item.get("type");
					_log.debug("Checking event type:{}",eventType);
					if( !skipEvents.containsKey(eventType)) {
						skipAll=false;
						break;
					}
				}
				_log.debug("Skip sending callpoll:{}",skipAll);
				skip=skipAll;
			}

			if( skip ){
				_log.debug("Skip forwarding event, save for later:{}",listPollEvents);
				if( pendingVssfPollEvents==null){
					pendingVssfPollEvents=new ArrayList<>();
				}
				pendingVssfPollEvents.addAll(listPollEvents);
				_log.debug("Total events saved:{}",pendingVssfPollEvents);
				return;
			} else {
				if( pendingVssfPollEvents!=null && pendingVssfPollEvents.size()>0 ){
					Map<String,Object> callPoll =(Map<String,Object>) ((Map<String,Object>)rawPollMessage.get("event"))
							.get("callPoll");
					listPollEvents = (List<Map<String,Object>>)callPoll.get("vssfPollEvents");
					_log.debug("Merge pending VSSF events:{}",pendingVssfPollEvents);
					listPollEvents.addAll(pendingVssfPollEvents);
				}
				pendingVssfPollEvents=null;
				if( this.lockEventId!=null) {
					rawPollMessage.put("event-id",this.lockEventId);
					_log.error("Execution time vssf session:{} :{}",this.getCallId(),Instant.now().toEpochMilli()-this.lockEventIdTime);
					this.lockEventId=null;
					this.lockEventIdTime=0;
				}

				RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
				getValidQueue();
				boolean status = RedisPool.get().sendToQueue(rawPollMessage, this.egressQueue);
				asn1Param.clear();

				if( this.vssfCallUser ){
					this.enableVssfSkip=false;
				}
			}




//			if( status ){
//	//			prepareResponsePoll(call.getId());
//				Map<String,Object> rawResponse = RedisPool.get().getFromQueue(ingressQueue);
//				JsonSCIFResponse jsonResponse = new JsonSCIFResponseCallPoll(call, rawResponse, jsonSession, anEvent);
//				//create fake timer which fires imediately
//				JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(call, jsonResponse);
//				_log.debug("Arming timer ...");
//				Timer timer = TelecomUtils.createTimer(timerListener, 0, this.appSession, TIMER_TYPE.ASYNCHRONOUS_CALL);
//	
//				_log.debug("RawResponse :{}",rawResponse);
//			}
		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callEnd(Cause aCause) {
		_log.debug("Removing callid:{}",this.callId);
		if( this.callId!=null ){
			calls.remove(this.callId);
		} else {

		}

		if( overload==true || this.callEndDone){
			return;
		}

		if( this.vssfCallUser==true ){
			String counter = "tas.cap.vssf.callEnd."+aCause;
			MetricsServer.get().counterIncrement(counter);
		} else {
			String counter = "tas.cap.callEnd."+aCause;
			MetricsServer.get().counterIncrement(counter);
		}


		try{
			_log.info("CallUser callEnd cause:{}", aCause );
			Map<String, Object> rawEnd = jsonCall.getCallEnd(aCause);
			CsSmsParameterSet smsParameterSet = this.call.getParameterSet(CsSmsParameterSet.class);
			if( smsParameterSet!=null && answered && this.vssfCallUser==false){
				_log.info("Skip sending SMS callEnd cause:{} overload:{}",aCause, this.overload);

				if((aCause==Cause.SESSION_TIMEOUT ||
						aCause==Cause.INTERNAL ||
						aCause==Cause.REJECTED)){

					try {
						String callRefNumber = smsParameterSet.getCallReferenceNumber();
						_log.error("SMS CallEnd internal for session:{} vssf:{} session:{} queue:{} callref:{}", this.getCallId(),
								this.vssfCallUser,
								this.session,
								this.egressQueue,
								callRefNumber);
					} catch (Exception e){
						_log.error("Exception loggging internal error");
					}
				}
				RedisProcessorThread.remove(jsonCall.getCallId());
				return ;
			}

			_log.debug(" rawEnd:{}",rawEnd);
			if( this.lockEventId!=null) {
				rawEnd.put("event-id",this.lockEventId);
				if( this.vssfCallUser ) {
					_log.error("Execution time vssf session:{} :{}", this.getCallId(), Instant.now().toEpochMilli() - this.lockEventIdTime);
				}
				this.lockEventId=null;
				this.lockEventIdTime=0;
			}

			if( session!=null ){
				rawEnd.put("session",session);
			}

			if( asn1Param!=null && asn1Param.size()>0) {
				rawEnd.put("asn1", asn1Param);
			}

			if( this.vssfCallUser==true ){
				//overwrite event-name
				String eventName=(String)rawEnd.get("event-name");
				if (eventName != null) {
					String suffix = "NULL";
					if( aCause!=null ){
						suffix=aCause.toString();
					}
					eventName = eventName + ".vssf."+suffix;
					rawEnd.put("event-name",eventName);
				}

				rawEnd.put("vssf",true);
			}

			getValidQueue();
			boolean status =  RedisPool.get().sendToQueue(rawEnd, this.egressQueue);
			asn1Param.clear();
		} finally {
			jsonCall.incrementEventId();
			this.callEndDone=true;

			if(this.overload==false && (aCause==Cause.SESSION_TIMEOUT ||
					aCause==Cause.INTERNAL ||
					aCause==Cause.REJECTED)){

				CsCallParameterSet csCall = this.call.getParameterSet(CsCallParameterSet.class);
				CsSmsParameterSet csSmsParameterSet = this.call.getParameterSet(CsSmsParameterSet.class);
				String callRefNumber="none";
				if( csCall!=null ) {
					callRefNumber = csCall.getCallReferenceNumber();
				}
				if( csSmsParameterSet!=null ){
					callRefNumber=csSmsParameterSet.getCallReferenceNumber();
				}
				_log.error("CallEnd internal for session:{} vssf:{} session:{} queue:{} callref:{}",this.getCallId(),
						this.vssfCallUser,
						this.session,
						this.egressQueue,
						callRefNumber);
			}

			//remove events from parking slot
			RedisProcessorThread.remove(jsonCall.getCallId());
		}
	}


	public String getCallId() {
		return jsonCall.getCallId();
	}



	public void setCall(Call newCall) {
		this.call= newCall;
	}

	
	private void prepareCallConnect(String callid, Map<String, Object> request){
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, Object> jsonCallStart = new HashMap<>();
		Map<String, Object> action = new HashMap<>();
		action.put("type",1);
		jsonCallStart.put("action", action);

		
		jsonCallStart.put("callid",callid);
		jsonCallStart.put("eventtime",Calendar.getInstance().getTimeInMillis());
		jsonCallStart.put("starttime", request.get("starttime"));
				
		jsonCallStart.put("queue", Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		jsonCallStart.put("as", Parameters.getImsName());
		//3. timestamp of the event
		
		Address destAddr = new Address();
		destAddr.setScheme("tel");
		destAddr.setUser("407222111000");
		destAddr.setParameter(Address.Parameter.NETWORK_CONTEXT , Address.TS24008Standard.encode(Address.TS24008Standard.Nai.INTERNATIONAL_NUMBER, Address.TS24008Standard.Npi.E164));


		Map<String, Object> destAddrJson = TelecomUtils.addressToJson(destAddr);
		action.put("uri",destAddrJson);
		
		destAddr = new Address();
		destAddr.setUser("40722000000");
		destAddr.setScheme("tel");
		destAddr.setParameter(Address.Parameter.NUMBER_QUALIFIER_IND, Address.NumberQualifier.ADDITIONAL_CALLING_PARTY_NUMBER);
		destAddr.setParameter(Address.Parameter.SCREENING , Address.Screening.USER_PROVIDED_VERIFIED_AND_PASSED);
		destAddr.setParameter(Address.Parameter.PRESENTATION , Address.Presentation.ALLOWED);
		destAddr.setParameter(Address.Parameter.NETWORK_CONTEXT , Address.TS24008Standard.encode(Address.TS24008Standard.Nai.INTERNATIONAL_NUMBER, Address.TS24008Standard.Npi.E164));
		
		Map<String, Object> cgPaAddrJson = TelecomUtils.addressToJson(destAddr);
		action.put("calling_number",cgPaAddrJson);
		jsonCallStart.put("id",0);
		jsonCallStart.put("eventname","callStart");


		getValidQueue();
		RedisPool.get().sendToQueue(jsonCallStart, Parameters.getImsName());

	}

	public Call getCall(){
		return this.call;
	}

	 public void setFireAndForget(Boolean fireAndForget) {
		this.fireAndFortget = fireAndForget;
	 }


	 public void makeCall( Map<String,Object> raw)  {
		this.vssfCallUser=true;
		 Map<String,Object> makeCall = (Map<String,Object>) raw.get("makeCall");

		 String media = (String)makeCall.get("media");
		 if( media==null || media.equalsIgnoreCase("voice")){
			 makeCallVoice(raw);
		 } else if (media.equalsIgnoreCase("sms")){
			 makeCallSMS(raw);
		 }
	 }

	 public void makeCallSMS( Map<String,Object> raw)  {
		 String counter = "tas.makeCall.sms";
		 MetricsServer.get().counterIncrement(counter);


		 Map<String,Object> makeCall = (Map<String,Object>) raw.get("makeCall");

		 CsCallParameterSet csPset = call.getParameterSet(CsCallParameterSet.class);
		 CsSmsParameterSet csSMSPset = call.getParameterSet(CsSmsParameterSet.class);
		 CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);

		 try {

			 if( makeCall.containsKey("serviceKey")){
				 call.getParameterSet(CommonParameterSet.class).setServiceKey(makeCall.get("serviceKey").toString());
			 }

			 csSMSPset.setNetworkProtocol(CsCommonParameterSubset.NetworkProtocol.CAMEL3);
			 if( makeCall.containsKey("networkProtocol")){
				 csSMSPset.setNetworkProtocol(CsCommonParameterSubset.NetworkProtocol.valueOf((String)makeCall.get("networkProtocol")));
			 }

			 Map<String,Object> sccpAddress = (Map<String,Object>) makeCall.get("sccpCalledParty");
			 Address sccpdestAddress = TelecomUtils.mapToAddr(sccpAddress);

			 _log.debug("makeCall scpAddress=" + sccpdestAddress);
			 csSMSPset.setSccpCalledPartyAddress(sccpdestAddress);

			 if( makeCall.containsKey("networkCallType")){
				 commPset.setNetworkCallType(CommonParameterSet.NetworkCallType.valueOf((String)makeCall.get("networkCallType")));
			 }


			 Map<String,Object> smscAddress = (Map<String,Object>) makeCall.get("smscAddress");
			 if( smscAddress!=null ) {
				 Address value = TelecomUtils.mapToAddr(smscAddress);
				 csSMSPset.setSmscAddress(value);
			 }
			 if(makeCall.containsKey("imsi")){
				 csSMSPset.setIMSI((String)makeCall.get("imsi"));
			 }
			 if(makeCall.containsKey("callReferenceNumber")) {
				 csSMSPset.setCallReferenceNumber((String)makeCall.get("callReferenceNumber"));
			 }
			 if(makeCall.containsKey("eventTypeSMS")) {
				 csSMSPset.setEventTypeSMS((Integer)makeCall.get("eventTypeSMS"));
			 }

			 Map<String,Object> vlrAddress = (Map<String,Object>)makeCall.get("vlrAddress");
			 if( vlrAddress!=null) {
				 Address value = TelecomUtils.mapToAddr(vlrAddress);
				 csSMSPset.setVlrAddress(value);
			 }

			 Map<String,Object> mscAddress = (Map<String,Object>)makeCall.get("mscAddress");
			 if( mscAddress!=null) {
				 Address value = TelecomUtils.mapToAddr(mscAddress);
				 csSMSPset.setMscAddress(value);
			 }

			 //TODO - set cellid
			 //TODO - set "shortMessageSpecificInfo":1,
			 //    "protocolIdentifier":0,
			 //    "dataCodingScheme":0,


			 if( makeCall.containsKey("timeAndTimezone")){
				 asn1TimeAndTimezone=(String)makeCall.get("timeAndTimezone");
			 }
			 if( makeCall.containsKey("protocolIdentifier")){
				 smsProtocolIdentif=(String)makeCall.get("protocolIdentifier");
			 }
			 if( makeCall.containsKey("dataCoding")){
				 smsDataCoding=(String)makeCall.get("dataCoding");
			 }
			 if( makeCall.containsKey("shortMessageSpecificInfo")){
				 smsShortMsgSecInfo=(String)makeCall.get("shortMessageSpecificInfo");
			 }
			 if( makeCall.containsKey("validityPeriod")){
				 smsValidityPeriod=(String)makeCall.get("validityPeriod");
			 }

			 List<Map<String,Object>> jsonExts = (List<Map<String,Object>>)makeCall.get("extensions");
			 if( jsonExts!=null && jsonExts.size()>0){
				 List<CsVendorExtensions> extensions = new ArrayList<CsVendorExtensions>();
				 for( Map<String,Object> item : jsonExts){
					 Integer extId = (Integer) item.get("id");

					 if( extId==4){
						 Integer value = (Integer) item.get("value");
						 PCallType pCallType = new PCallType(CsVendorExtensions.CsOperationCode.INITIALDPSMS, CsVendorExtensions.Criticality.IGNORE, CsVendorExtensions.PExtension.CallType.values()[value]);
						 extensions.add(pCallType);
					 } else if (extId==5) {
						 Object valueObj = item.get("value");
						 Integer value=0;
						 if( valueObj instanceof  Double){
							 value = ((Double) valueObj).intValue();
						 } else if ( valueObj instanceof Long){
							 value = ((Long) valueObj).intValue();
						 } else if ( valueObj instanceof Integer){
							 value = (Integer) valueObj;
						 }

						 CsVendorExtensions.PExtension.ChargeAcctType chargeAcctType = CsVendorExtensions.PExtension.ChargeAcctType.values()[value];
						 PChargeAccountType pChargeAccountType = new PChargeAccountType(CsVendorExtensions.CsOperationCode.INITIALDPSMS,CsVendorExtensions.Criticality.IGNORE,chargeAcctType);
						 extensions.add(pChargeAccountType);
					 } else if (extId==6) {
						 String value = (String) item.get("value");
						 PNumberPortabilityPrefix pnp = new PNumberPortabilityPrefix(CsVendorExtensions.CsOperationCode.INITIALDPSMS, CsVendorExtensions.Criticality.IGNORE, value);
						 extensions.add(pnp);
					 } else if (extId==7) {
						 String value = (String) item.get("value");
						 PVmcrPrefix pVmcrPrefix = new PVmcrPrefix(CsVendorExtensions.CsOperationCode.INITIALDPSMS, CsVendorExtensions.Criticality.IGNORE, value);
						 extensions.add(pVmcrPrefix);
					 } else if (extId==1){
						 Integer value = (Integer) item.get("value");
						 PServiceScenarioIdentifier pServiceScenarioIdentifier = new PServiceScenarioIdentifier(CsVendorExtensions.CsOperationCode.INITIALDPSMS, CsVendorExtensions.Criticality.IGNORE, value);
						 extensions.add(pServiceScenarioIdentifier);
					 }
				 }
				 csSMSPset.setCsVendorExtensions(extensions);
			 }

		 } catch (ScifException e) {
			 _log.error("Unable to set parameters", e);
		 }

		 Map<String,Object> callingParty = (Map<String,Object>)makeCall.get("callingParty");
		 Address cgPa=TelecomUtils.mapToAddr(callingParty);

		 Map<String,Object> calledParty = (Map<String,Object>)makeCall.get("calledParty");
		 Address cdPa=TelecomUtils.mapToAddr(calledParty);
		 TerminalContact terminalContact = new TerminalContact(cdPa);


		 try {
			 call.makeCall(cgPa, terminalContact);
		 } catch (ScifException e) {
			 _log.error("Unable to make the Call", e);
		 }


	 }
	 public void makeCallVoice( Map<String,Object> raw)  {
		 Map<String,Object> makeCall = (Map<String,Object>) raw.get("makeCall");

		 CsCallParameterSet csPset = call.getParameterSet(CsCallParameterSet.class);
		 CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);

		 _log.debug("Make call...:{}",makeCall);


		 String counter = "tas.makeCall.voice";
		 MetricsServer.get().counterIncrement(counter);
		 try {

			if( makeCall.containsKey("serviceKey")){
				call.getParameterSet(CommonParameterSet.class).setServiceKey(makeCall.get("serviceKey").toString());
			}

			csPset.setNetworkProtocol(CsCommonParameterSubset.NetworkProtocol.CAMEL2);
			Map<String,Object> sccpAddress = (Map<String,Object>) makeCall.get("sccpCalledParty");
			Address sccpdestAddress = TelecomUtils.mapToAddr(sccpAddress);

			_log.debug("makeCall scpAddress=" + sccpdestAddress);
			csPset.setSccpCalledPartyAddress(sccpdestAddress);



			if( makeCall.containsKey("sccpCallingParty")) {
				Map<String, Object> sccpCgPaAddress = (Map<String, Object>) makeCall.get("sccpCallingParty");
				Address sccporiginAddress = TelecomUtils.mapToAddr(sccpCgPaAddress);

				_log.debug("makeCall scpCgPaAddress=" + sccporiginAddress);


			}

			Map<String,Object> locationNumber = (Map<String,Object>) makeCall.get("locationNumber");
			if( locationNumber!=null ) {
				Address locationNumberAddr = TelecomUtils.mapToAddr(locationNumber);
				csPset.setLocationNumber(locationNumberAddr);
			}
			if( makeCall.containsKey("callingPartysCategory")) {
				csPset.setCallingPartysCategory((Integer) makeCall.get("callingPartysCategory"));
			}
			if(makeCall.containsKey("highLayerCharacteristics")) {
				csPset.setHighLayerCharacteristics((Integer) makeCall.get("highLayerCharacteristics"));
			}
			if(makeCall.containsKey("eventTypeBcsm")) {
				csPset.setEventTypeBCSM((Integer) makeCall.get("eventTypeBcsm"));
			}
			if(makeCall.containsKey("callReferenceNumber")) {
				csPset.setCallReferenceNumber((String)makeCall.get("callReferenceNumber"));
			}
			if(makeCall.containsKey("imsi")){
				csPset.setIMSI((String)makeCall.get("imsi"));
			}

			 if( makeCall.containsKey("timeAndTimezone")){
				 asn1TimeAndTimezone=(String)makeCall.get("timeAndTimezone");
			 }

			if(makeCall.containsKey("rawBearerCapability")) {
				csPset.setRawBearerCapability(DatatypeConverter.parseHexBinary((String)makeCall.get("rawBearerCapability")));
			}

			if( makeCall.containsKey("networkProtocol")){
				csPset.setNetworkProtocol(CsCommonParameterSubset.NetworkProtocol.valueOf((String)makeCall.get("networkProtocol")));
			}

			Map<String,Object> mscAddress = (Map<String,Object>)makeCall.get("mscAddress");
			if( mscAddress!=null) {
				Address mscAddr = TelecomUtils.mapToAddr(mscAddress);
				csPset.setMscAddress(mscAddr);
			}

			 Map<String,Object> originalCdPA = (Map<String,Object>)makeCall.get("originalCalledPartyID");
			 if( originalCdPA!=null) {
				 Address value = TelecomUtils.mapToAddr(originalCdPA);
				 csPset.setOriginalCalledPartyID(value);
			 }
			 Map<String,Object> redirectingPartyID = (Map<String,Object>)makeCall.get("redirectingPartyID");
			 if( redirectingPartyID!=null) {
				 Address value = TelecomUtils.mapToAddr(redirectingPartyID);
				 commPset.setRedirectingParty(value);
			 }

			 Map<String,Object> addCgPA = (Map<String,Object>)makeCall.get("additionalCallingPartyNumber");
			 if( addCgPA!=null) {
				 Address value = TelecomUtils.mapToAddr(addCgPA);
				 //TODO - set additional calling party
				 //not possible to set additional calling party

			 }

			 if( makeCall.containsKey("redirectionInformation")){
				 csPset.setRawRedirectionInformation(DatatypeConverter.parseHexBinary((String)makeCall.get("redirectionInformation")));
			 }

//			 if( makeCall.containsKey("timeAndTimezone")){
//				 csPset.setTimeZoneOffSet((Integer)makeCall.get("timeAndTimezone"));
//			 }

			 if(makeCall.containsKey("bearerCapability")){
				 csPset.setRawBearerCapability(DatatypeConverter.parseHexBinary((String)makeCall.get("bearerCapability")));
			 }

			 if( makeCall.containsKey("ipspCapabilities")){
				 csPset.setRawIPSSPCapabilities(DatatypeConverter.parseHexBinary((String)makeCall.get("ipspCapabilities")));
			 }

			 if( makeCall.containsKey("teleService")){
				 csPset.setExtnTeleServiceCode((String)makeCall.get("teleService"));
			 }

			 Map<String,Object> vlrAddress = (Map<String,Object>)makeCall.get("vlrAddress");
			 if( vlrAddress!=null) {
				 Address value = TelecomUtils.mapToAddr(vlrAddress);
				 csPset.setVlrAddress(value);
				 _log.debug("VSSF set vlr address:{}",value.toString());
			 }

			 asn1LocationInformation = (Map<String, Object>) makeCall.get("locationInformation");

			 List<Map<String,Object>> jsonExts = (List<Map<String,Object>>)makeCall.get("extensions");
			 if( jsonExts!=null && jsonExts.size()>0){
				 List<CsVendorExtensions> extensions = new ArrayList<CsVendorExtensions>();
				 for( Map<String,Object> item : jsonExts){
					 Integer extId = (Integer) item.get("id");

					 if( extId==4){
						 Integer value = (Integer) item.get("value");
						 PCallType pCallType = new PCallType(CsVendorExtensions.CsOperationCode.INITIALDP, CsVendorExtensions.Criticality.IGNORE, CsVendorExtensions.PExtension.CallType.values()[value]);
						 extensions.add(pCallType);
					 } else if (extId==5) {
						 Object valueObj =  item.get("value");
						 Integer value=0;
						 if( valueObj instanceof  Double){
							 value = ((Double) valueObj).intValue();
						 } else if ( valueObj instanceof Long){
							 value = ((Long) valueObj).intValue();
						 } else if ( valueObj instanceof Integer){
							 value = (Integer) valueObj;
						 }
						 CsVendorExtensions.PExtension.ChargeAcctType chargeAcctType = CsVendorExtensions.PExtension.ChargeAcctType.values()[value];
						 PChargeAccountType pChargeAccountType = new PChargeAccountType(CsVendorExtensions.CsOperationCode.INITIALDP,CsVendorExtensions.Criticality.IGNORE,chargeAcctType);
						 extensions.add(pChargeAccountType);
					 } else if (extId==6) {
						 String value = (String) item.get("value");
						 PNumberPortabilityPrefix pnp = new PNumberPortabilityPrefix(CsVendorExtensions.CsOperationCode.INITIALDP, CsVendorExtensions.Criticality.IGNORE, value);
						 extensions.add(pnp);
					 } else if (extId==7) {
						 String value = (String) item.get("value");
						 PVmcrPrefix pVmcrPrefix = new PVmcrPrefix(CsVendorExtensions.CsOperationCode.INITIALDP, CsVendorExtensions.Criticality.IGNORE, value);
						 extensions.add(pVmcrPrefix);
					 } else if (extId==1){
						 Integer value = (Integer) item.get("value");
						 PServiceScenarioIdentifier pServiceScenarioIdentifier = new PServiceScenarioIdentifier(CsVendorExtensions.CsOperationCode.INITIALDP, CsVendorExtensions.Criticality.IGNORE, value);
						 extensions.add(pServiceScenarioIdentifier);
					 }
				 }
				 csPset.setCsVendorExtensions(extensions);
			 }
		 } catch (ScifException e) {
			 _log.error("Unable to set parameters", e);
		 }

		 Map<String,Object> callingParty = (Map<String,Object>)makeCall.get("callingParty");
		 Address cgPa=null;
		 if( callingParty!=null) {
			 cgPa = TelecomUtils.mapToAddr(callingParty);
		 } /*else {
			 cgPa = new Address();
			 cgPa.setScheme("tel");
			 cgPa.setUser("0000");
			 cgPa.setParameter("network-context","Q763.2.1");
		 }*/

		 Map<String,Object> calledParty = (Map<String,Object>)makeCall.get("calledParty");
		 Address cdPa=TelecomUtils.mapToAddr(calledParty);
		 TerminalContact terminalContact = new TerminalContact(cdPa);

		 try {
			 _log.debug("CgPA:{}",cgPa);
			 call.makeCall(cgPa, terminalContact);
		 } catch (ScifException e) {
			 _log.error("Unable to make the Call", e);
		 }
	 }

	 public void setQueue(String value){
		 _log.debug("Set queue affinity:{}",value);
		 ConsumersCache.get().setConsumer(value);
		 this.lastQueueSetTime = Instant.now().toEpochMilli();
		 this.egressQueue =value;
	 }

	 @Override
	 public void messageReceived(Object o, CallLeg callLeg) {
		 InMessageInterceptorExtn msg = (InMessageInterceptorExtn)o;

		_log.debug("messageReceived opcode:{} pdu:{}",msg.getOperationCode(),msg.getPdu());
		try {
			if (msg.getOperationCode() == 60) {
				try {
					InitialDPSMSArg idpsms = (InitialDPSMSArg) msg.getPdu();
					if (idpsms.getLocationInformationMSC() != null &&
							idpsms.getLocationInformationMSC().getLocationNumber() != null) {
						asn1Param.put("locationNumber", DatatypeConverter.printHexBinary(idpsms.getLocationInformationMSC().getLocationNumber().byteArrayValue()));
					}
					if (idpsms.hasTimeAndTimezone()) {
						asn1Param.put("timeAndTimeZone", DatatypeConverter.printHexBinary(idpsms.getTimeAndTimezone().byteArrayValue()));
					}
					if( idpsms.hasTPDataCodingScheme()){
						asn1Param.put("tpDataCodingScheme", DatatypeConverter.printHexBinary(idpsms.getTPDataCodingScheme().byteArrayValue()));
					}
					if( idpsms.hasTPProtocolIdentifier()){
						asn1Param.put("tpProtocolIdentifier", DatatypeConverter.printHexBinary(idpsms.getTPProtocolIdentifier().byteArrayValue()));
					}
					if( idpsms.hasTPShortMessageSubmissionInfo()){
						asn1Param.put("tpShortMessageSubmissionInfo", DatatypeConverter.printHexBinary(idpsms.getTPShortMessageSubmissionInfo().byteArrayValue()));
					}
					if( idpsms.hasTPValidityPeriod()){
						asn1Param.put("tpValidityPeriod", DatatypeConverter.printHexBinary(idpsms.getTPValidityPeriod().byteArrayValue()));
					}

				} catch (Exception e) {
					_log.error("Exception", e);
				}

			} else if (msg.getOperationCode() == 20) {
				Object pdu = msg.getPdu();
				if( pdu instanceof ConnectArg){
					ConnectArg con = (ConnectArg) pdu;
					if( con.hasOriginalCalledPartyID() ){
						asn1Param.put("originalCalledPartyID",DatatypeConverter.printHexBinary(con.getOriginalCalledPartyID().byteArrayValue()));
					}
					if( con.hasRedirectingPartyID() ){
						asn1Param.put("redirectingPartyID",DatatypeConverter.printHexBinary(con.getRedirectingPartyID().byteArrayValue()));
					}
					if( con.hasRedirectionInformation() ){
						asn1Param.put("redirectionInformation",DatatypeConverter.printHexBinary(con.getRedirectionInformation().byteArrayValue()));
					}
					OctetString dest = con.getDestinationRoutingAddress().get(0);
					asn1Param.put("destination",DatatypeConverter.printHexBinary(dest.byteArrayValue()));
				} else if( pdu instanceof com.hp.opencall.camel.camel2.cap_datatypes.ConnectArg ){
					com.hp.opencall.camel.camel2.cap_datatypes.ConnectArg con = (com.hp.opencall.camel.camel2.cap_datatypes.ConnectArg) pdu;
					if( con.hasOriginalCalledPartyID() ){
						asn1Param.put("originalCalledPartyID",DatatypeConverter.printHexBinary(con.getOriginalCalledPartyID().byteArrayValue()));
					}
					if( con.hasRedirectingPartyID() ){
						asn1Param.put("redirectingPartyID",DatatypeConverter.printHexBinary(con.getRedirectingPartyID().byteArrayValue()));
					}
					if( con.hasRedirectionInformation() ){
						asn1Param.put("redirectionInformation",DatatypeConverter.printHexBinary(con.getRedirectionInformation().byteArrayValue()));
					}
					OctetString dest = con.getDestinationRoutingAddress().get(0);
					asn1Param.put("destination",DatatypeConverter.printHexBinary(dest.byteArrayValue()));

				} else if( pdu instanceof com.hp.opencall.camel.camel1.cap_datatypes.ConnectArg){
					com.hp.opencall.camel.camel1.cap_datatypes.ConnectArg con = (com.hp.opencall.camel.camel1.cap_datatypes.ConnectArg) pdu;
					if( con.hasOriginalCalledPartyID() ){
						asn1Param.put("originalCalledPartyID",DatatypeConverter.printHexBinary(con.getOriginalCalledPartyID().byteArrayValue()));
					}
					if( con.hasRedirectingPartyID() ){
						asn1Param.put("redirectingPartyID",DatatypeConverter.printHexBinary(con.getRedirectingPartyID().byteArrayValue()));
					}
					if( con.hasRedirectionInformation() ){
						asn1Param.put("redirectionInformation",DatatypeConverter.printHexBinary(con.getRedirectionInformation().byteArrayValue()));
					}
					OctetString dest = con.getDestinationRoutingAddress().get(0);
					asn1Param.put("destination",DatatypeConverter.printHexBinary(dest.byteArrayValue()));

				}
			} else if (msg.getOperationCode() == 0) {

				Object pdu = msg.getPdu();

				try {
					if (pdu instanceof com.hp.opencall.camel.camel3.oc_camel3_pdus.InitialDPArg) {
						InitialDPArg idp = (InitialDPArg) msg.getPdu();
						if (idp.hasTimeAndTimezone()) {
							asn1Param.put("timeAndTimeZone", DatatypeConverter.printHexBinary(idp.getTimeAndTimezone().byteArrayValue()));
						}
						if (idp.hasBearerCapability()) {
							asn1Param.put("bearerCapability", DatatypeConverter.printHexBinary(idp.getBearerCapability().getBearerCap().byteArrayValue()));
						}
						if( idp.hasLocationInformation()){
							Map<String,Object> locationInformation = new HashMap<>();
							if ( idp.getLocationInformation().hasAgeOfLocationInformation() ){
								locationInformation.put("ageOfLocation",idp.getLocationInformation().getAgeOfLocationInformation().intValue());
							}
							if( idp.getLocationInformation().hasCellGlobalIdOrServiceAreaIdOrLAI()){
								if( idp.getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().hasCellGlobalIdOrServiceAreaIdFixedLength()){
									locationInformation.put("cellIdSAI",DatatypeConverter.printHexBinary(idp.getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().getCellGlobalIdOrServiceAreaIdFixedLength().byteArrayValue()));
								} else if (idp.getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().hasLaiFixedLength()){
									locationInformation.put("cellIdLAI",DatatypeConverter.printHexBinary(idp.getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().getLaiFixedLength().byteArrayValue()));
								}
							}
							if( idp.getLocationInformation().hasSai_Present()){
								locationInformation.put("saiPresent",true);
							}

							if( locationInformation.size()>0){
								asn1Param.put("locationInformation",locationInformation);
							}
						}
					} else if (pdu instanceof com.hp.opencall.camel.camel2.cap_datatypes.InitialDPArg) {
						com.hp.opencall.camel.camel2.cap_datatypes.InitialDPArg idp = (com.hp.opencall.camel.camel2.cap_datatypes.InitialDPArg) msg.getPdu();
						if (idp.hasTimeAndTimezone()) {
							asn1Param.put("timeAndTimeZone", DatatypeConverter.printHexBinary(idp.getTimeAndTimezone().byteArrayValue()));
						}
						if (idp.hasBearerCapability()) {
							asn1Param.put("bearerCapability", DatatypeConverter.printHexBinary(idp.getBearerCapability().getBearerCap().byteArrayValue()));
						}
						if( idp.hasLocationInformation()){
							Map<String,Object> locationInformation = new HashMap<>();
							if ( idp.getLocationInformation().hasAgeOfLocationInformation() ){
								locationInformation.put("ageOfLocation",idp.getLocationInformation().getAgeOfLocationInformation().intValue());
							}
							if( idp.getLocationInformation().hasCellIdOrLAI()){
								if( idp.getLocationInformation().getCellIdOrLAI().hasCellIdFixedLength()){
									locationInformation.put("cellId",DatatypeConverter.printHexBinary(idp.getLocationInformation().getCellIdOrLAI().getCellIdFixedLength().byteArrayValue()));
								} else if (idp.getLocationInformation().getCellIdOrLAI().hasLaiFixedLength()){
									locationInformation.put("cellIdLAI",DatatypeConverter.printHexBinary(idp.getLocationInformation().getCellIdOrLAI().getLaiFixedLength().byteArrayValue()));
								}
							}
							if( locationInformation.size()>0){
								asn1Param.put("locationInformation",locationInformation);
							}
						}
					} else if (pdu instanceof com.hp.opencall.camel.camel1.cap_datatypes.InitialDPArg ) {
						com.hp.opencall.camel.camel1.cap_datatypes.InitialDPArg idp = (com.hp.opencall.camel.camel1.cap_datatypes.InitialDPArg) msg.getPdu();
						if (idp.hasBearerCapability()) {
							asn1Param.put("bearerCapability", DatatypeConverter.printHexBinary(idp.getBearerCapability().getBearerCap().byteArrayValue()));
						}
						if( idp.hasLocationInformation()){
							Map<String,Object> locationInformation = new HashMap<>();
							if ( idp.getLocationInformation().hasAgeOfLocationInformation() ){
								locationInformation.put("ageOfLocation",idp.getLocationInformation().getAgeOfLocationInformation().intValue());
							}
							if( idp.getLocationInformation().hasCellIdOrLAI()){
								if( idp.getLocationInformation().getCellIdOrLAI().hasCellIdFixedLength()){
									locationInformation.put("cellId",DatatypeConverter.printHexBinary(idp.getLocationInformation().getCellIdOrLAI().getCellIdFixedLength().byteArrayValue()));
								} else if (idp.getLocationInformation().getCellIdOrLAI().hasLaiFixedLength()){
									locationInformation.put("cellIdLAI",DatatypeConverter.printHexBinary(idp.getLocationInformation().getCellIdOrLAI().getLaiFixedLength().byteArrayValue()));
								}
							}
							if( locationInformation.size()>0){
								asn1Param.put("locationInformation",locationInformation);
							}
						}
					}
				} catch (Exception e) {
					_log.error("Exception", e);
				}
			} else if (msg.getOperationCode() == 17) {
				Object pdu = msg.getPdu();
				if (pdu instanceof com.hp.opencall.camel.camel3.oc_camel3_pdus.EstablishTemporaryConnectionArg) {
					com.hp.opencall.camel.camel3.oc_camel3_pdus.EstablishTemporaryConnectionArg etc = (com.hp.opencall.camel.camel3.oc_camel3_pdus.EstablishTemporaryConnectionArg) pdu;
					if( etc.hasCorrelationID() ) {
						asn1Param.put("correlationId", DatatypeConverter.printHexBinary(etc.getCorrelationID().byteArrayValue()));
					}
					if( etc.hasScfID() ){
						asn1Param.put("scfId", DatatypeConverter.printHexBinary(etc.getScfID().byteArrayValue()));
					}
					asn1Param.put("assistingSSPIPRoutingAddress",DatatypeConverter.printHexBinary(etc.getAssistingSSPIPRoutingAddress().byteArrayValue()));
				} else if (pdu instanceof com.hp.opencall.camel.camel2.cap_datatypes.EstablishTemporaryConnectionArg) {
					com.hp.opencall.camel.camel2.cap_datatypes.EstablishTemporaryConnectionArg etc = (com.hp.opencall.camel.camel2.cap_datatypes.EstablishTemporaryConnectionArg) pdu;
					if( etc.hasCorrelationID()) {
						asn1Param.put("correlationId", DatatypeConverter.printHexBinary(etc.getCorrelationID().byteArrayValue()));
					}
					if( etc.hasScfID() ){
						asn1Param.put("scfId", DatatypeConverter.printHexBinary(etc.getScfID().byteArrayValue()));
					}
					asn1Param.put("assistingSSPIPRoutingAddress",DatatypeConverter.printHexBinary(etc.getAssistingSSPIPRoutingAddress().byteArrayValue()));
				}
			} else if (msg.getOperationCode()==47){
				Object pdu = msg.getPdu();
				if(pdu instanceof PlayAnnouncementArg){
					PlayAnnouncementArg pa = (PlayAnnouncementArg) pdu;
					if (pa.getInformationToSend()!=null && pa.getInformationToSend().hasInbandInfo()) {
						if (pa.getInformationToSend().getInbandInfo().getMessageID().hasVariableMessage()) {
							Map<String, Object> variables = new HashMap<>();
							final int[] cnt = {0};
							PlayAnnouncementArg.InformationToSend.InbandInfo.MessageID.VariableMessage.VariableParts variableParts = pa.getInformationToSend().getInbandInfo().getMessageID().getVariableMessage().getVariableParts();
							variableParts.forEach(item -> {
								String key = "key_" + cnt[0];
								cnt[0]++;
								Map<String, Object> var = new HashMap<>();
								var.put("type", item.getTypeInfo().getName());
								if (item.hasDate()) {
									var.put("date", DatatypeConverter.printHexBinary(item.getDate().byteArrayValue()));
									var.put("value", "date:" + var.get("date"));
								}
								if (item.hasInteger()) {
									var.put("integer", item.toString());
									var.put("value", "count:" + item.toString());
								}
								if (item.hasNumber()) {
									var.put("digit", DatatypeConverter.printHexBinary(item.getNumber().byteArrayValue()));
									var.put("value", "digit:" + var.get("digit"));
								}
								if (item.hasPrice()) {
									var.put("price", DatatypeConverter.printHexBinary(item.getPrice().byteArrayValue()));
									var.put("value", "currency:" + var.get("price"));
								}
								if (item.hasTime()) {
									var.put("time", DatatypeConverter.printHexBinary(item.getTime().byteArrayValue()));
									var.put("value", "time:" + var.get("time"));
								}

								if (item.hasUnknownExtension()) {
									var.put("unknown", "none");
								}
								variables.put(key, var);
							});
							if (variables.size() > 0) {
								asn1Param.put("variables", variables);
							}
						}
						if( pa.hasDisconnectFromIPForbidden() ){
							asn1Param.put("disconnectFromIPForbidden",pa.getDisconnectFromIPForbidden());
						}
					}
				}
			} else if (msg.getOperationCode()==48) {
				Object pdu = msg.getPdu();
				PromptAndCollectUserInformationArg pac = (PromptAndCollectUserInformationArg) pdu;
				if (pac.getInformationToSend() != null && pac.getInformationToSend().hasInbandInfo()) {
					if (pac.getInformationToSend().getInbandInfo().getMessageID().hasVariableMessage()) {
						Map<String, Object> variables = new HashMap<>();
						final int[] cnt = {0};
						PromptAndCollectUserInformationArg.InformationToSend.InbandInfo.MessageID.VariableMessage.VariableParts variableParts = pac.getInformationToSend().getInbandInfo().getMessageID().getVariableMessage().getVariableParts();
						variableParts.forEach(item -> {
							String key = "key_" + cnt[0];
							cnt[0]++;
							Map<String, Object> var = new HashMap<>();
							var.put("type", item.getTypeInfo().getName());
							if (item.hasDate()) {
								var.put("date", DatatypeConverter.printHexBinary(item.getDate().byteArrayValue()));
								var.put("value", "date:" + var.get("date"));
							}
							if (item.hasInteger()) {
								var.put("integer", item.toString());
								var.put("value", "count:" + item.toString());
							}
							if (item.hasNumber()) {
								var.put("digit", DatatypeConverter.printHexBinary(item.getNumber().byteArrayValue()));
								var.put("value", "digit:" + var.get("digit"));
							}
							if (item.hasPrice()) {
								var.put("price", DatatypeConverter.printHexBinary(item.getPrice().byteArrayValue()));
								var.put("value", "currency:" + var.get("price"));
							}
							if (item.hasTime()) {
								var.put("time", DatatypeConverter.printHexBinary(item.getTime().byteArrayValue()));
								var.put("value", "time:" + var.get("time"));
							}

							if (item.hasUnknownExtension()) {
								var.put("unknown", "none");
							}
							variables.put(key, var);
						});
						if (variables.size() > 0) {
							asn1Param.put("variables", variables);
						}
					}
					if( pac.hasDisconnectFromIPForbidden() ){
						asn1Param.put("disconnectFromIPForbidden",pac.getDisconnectFromIPForbidden());
					}

				}
			} else if (msg.getOperationCode()==24) {
				Object pdu = msg.getPdu();
				Map<String,Object> erbJson = new HashMap<>();
				if( pdu instanceof com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg ){
					com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg erb3 = (com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg) pdu;
					erbJson.put("erb",erb3.getEventTypeBCSM().longValue());

					if( erb3.hasMiscCallInfo()){
						erbJson.put("miscMessageType",erb3.getMiscCallInfo().getMessageType().longValue());
					}
					if( erb3.hasLegID() && erb3.getLegID().hasReceivingSideID()){
						com.hp.opencall.camel.common.cs1_datatypes.LegType type = erb3.getLegID().getReceivingSideID();
						erbJson.put("recvLegId",type.byteArrayValue()[0]);
					}
					//get specific information
					if( erb3.hasEventSpecificInformationBCSM() ){
						com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM specificInfo = erb3.getEventSpecificInformationBCSM();
						if( specificInfo.hasOAnswerSpecificInfo() ){
							//this is oAnswer, nothing special
						} else if ( specificInfo.hasODisconnectSpecificInfo() ){
							if( specificInfo.getODisconnectSpecificInfo().hasReleaseCause() ){
								String cause= DatatypeConverter.printHexBinary(specificInfo.getODisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause",cause);
							}
						} else if ( specificInfo.hasONoAnswerSpecificInfo() ) {
							//nothing to be done
						} else if ( specificInfo.hasOCalledPartyBusySpecificInfo() ){
							if( specificInfo.getOCalledPartyBusySpecificInfo().hasBusyCause()){
								String cause= DatatypeConverter.printHexBinary(specificInfo.getOCalledPartyBusySpecificInfo().getBusyCause().byteArrayValue());
								erbJson.put("cause",cause);
							}
						} else if ( specificInfo.hasTAnswerSpecificInfo() ){
							//do nothinh
						} else if ( specificInfo.hasTBusySpecificInfo() ) {
							if (specificInfo.getTBusySpecificInfo().hasBusyCause()){
								String cause= DatatypeConverter.printHexBinary(specificInfo.getTBusySpecificInfo().getBusyCause().byteArrayValue());
								erbJson.put("cause",cause);
							}
						} else if ( specificInfo.hasTDisconnectSpecificInfo() ){
							if ( specificInfo.getTDisconnectSpecificInfo().hasReleaseCause() ){
								String cause= DatatypeConverter.printHexBinary(specificInfo.getTDisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause",cause);
							}
						} else if ( specificInfo.hasTNoAnswerSpecificInfo() ){
							//
						} else if ( specificInfo.hasRouteSelectFailureSpecificInfo()){
							if( specificInfo.getRouteSelectFailureSpecificInfo().hasFailureCause()){
								String cause= DatatypeConverter.printHexBinary(specificInfo.getRouteSelectFailureSpecificInfo().getFailureCause().byteArrayValue());
								erbJson.put("cause",cause);
							}
						}
					}
				} else if( pdu instanceof EventReportBCSMArg ) {
					EventReportBCSMArg erb2 = (EventReportBCSMArg) pdu;
					erbJson.put("erb", erb2.getEventTypeBCSM().longValue());
					if( erb2.hasMiscCallInfo()){
						erbJson.put("miscMessageType",erb2.getMiscCallInfo().getMessageType().longValue());
					}
					if( erb2.hasLegID() && erb2.getLegID().hasReceivingSideID()){
						com.hp.opencall.camel.common.cs1_datatypes.LegType type = erb2.getLegID().getReceivingSideID();
						erbJson.put("recvLegId",type.byteArrayValue()[0]);
					}
					//get specific information
					if (erb2.hasEventSpecificInformationBCSM()) {
						EventSpecificInformationBCSM specificInfo = erb2.getEventSpecificInformationBCSM();
						if (specificInfo.hasOAnswerSpecificInfo()) {
							//this is oAnswer, nothing special
						} else if (specificInfo.hasODisconnectSpecificInfo()) {
							if (specificInfo.getODisconnectSpecificInfo().hasReleaseCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getODisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						} else if (specificInfo.hasONoAnswerSpecificInfo()) {
							//nothing to be done
						} else if (specificInfo.hasOCalledPartyBusySpecificInfo()) {
							if (specificInfo.getOCalledPartyBusySpecificInfo().hasBusyCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getOCalledPartyBusySpecificInfo().getBusyCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						} else if (specificInfo.hasTAnswerSpecificInfo()) {
							//do nothing
						} else if (specificInfo.hasTBusySpecificInfo()) {
							if (specificInfo.getTBusySpecificInfo().hasBusyCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getTBusySpecificInfo().getBusyCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						} else if (specificInfo.hasTDisconnectSpecificInfo()) {
							if (specificInfo.getTDisconnectSpecificInfo().hasReleaseCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getTDisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						} else if (specificInfo.hasTNoAnswerSpecificInfo()) {
							//
						} else if (specificInfo.hasRouteSelectFailureSpecificInfo()) {
							if (specificInfo.getRouteSelectFailureSpecificInfo().hasFailureCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getTDisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						}
					}

				} else 	if( pdu instanceof com.hp.opencall.camel.camel1.cap_datatypes.EventReportBCSMArg ) {
					com.hp.opencall.camel.camel1.cap_datatypes.EventReportBCSMArg erb1 = (com.hp.opencall.camel.camel1.cap_datatypes.EventReportBCSMArg) pdu;
					erbJson.put("erb", erb1.getEventTypeBCSM().longValue());
					if( erb1.hasLegID() && erb1.getLegID().hasReceivingSideID()){
						LegType type = erb1.getLegID().getReceivingSideID();
						erbJson.put("recvLegId",type.byteArrayValue()[0]);
					}
					if( erb1.hasLegID() && erb1.getLegID().hasSendingSideID()){
						LegType type = erb1.getLegID().getSendingSideID();
						erbJson.put("sendLegId",type.byteArrayValue()[0]);
					}

					if( erb1.hasMiscCallInfo()){
						erbJson.put("miscMessageType",erb1.getMiscCallInfo().getMessageType().longValue());
					}


					//get specific information
					if (erb1.hasEventSpecificInformationBCSM()) {
						com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM specificInfo = erb1.getEventSpecificInformationBCSM();
						if (specificInfo.hasODisconnectSpecificInfo()) {
							if (specificInfo.getODisconnectSpecificInfo().hasReleaseCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getODisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						} else if (specificInfo.hasTDisconnectSpecificInfo()) {
							if (specificInfo.getTDisconnectSpecificInfo().hasReleaseCause()) {
								String cause = DatatypeConverter.printHexBinary(specificInfo.getTDisconnectSpecificInfo().getReleaseCause().byteArrayValue());
								erbJson.put("cause", cause);
							}
						}
					}
				}

				asn1Param.put("erb", erbJson);
			}  else if (msg.getOperationCode()==66) {
				Object pdu = msg.getPdu();
				if( pdu instanceof ReleaseSMSArg) {
					ReleaseSMSArg relSMS = (ReleaseSMSArg) pdu;
					asn1Param.put("cause", DatatypeConverter.printHexBinary(relSMS.byteArrayValue()));
				}
			} else if (msg.getOperationCode()==22) {
				Object pdu = msg.getPdu();
				if( pdu instanceof ReleaseCallArg ) {
					ReleaseCallArg relCall = (ReleaseCallArg) pdu;
					asn1Param.put("cause", DatatypeConverter.printHexBinary(relCall.byteArrayValue()));
				} else if ( pdu instanceof com.hp.opencall.camel.camel2.cap_datatypes.ReleaseCallArg ){
					com.hp.opencall.camel.camel2.cap_datatypes.ReleaseCallArg relCall = (com.hp.opencall.camel.camel2.cap_datatypes.ReleaseCallArg) pdu;
					asn1Param.put("cause", DatatypeConverter.printHexBinary(relCall.byteArrayValue()));
				} else if ( pdu instanceof com.hp.opencall.camel.camel3.oc_camel3_pdus.ReleaseCallArg){
					com.hp.opencall.camel.camel3.oc_camel3_pdus.ReleaseCallArg relCall = (com.hp.opencall.camel.camel3.oc_camel3_pdus.ReleaseCallArg) pdu;
					asn1Param.put("cause", DatatypeConverter.printHexBinary(relCall.byteArrayValue()));
				}
			}


			_log.debug("Message received ASN1:{}",asn1Param);

		 }catch (Exception e){
			_log.error("Decode PDU Exception",e);
		}
	 }

	 @Override
	 public void sendingMessage(Object o, CallLeg callLeg) throws ScifException {
		 InMessageInterceptorExtn msg = (InMessageInterceptorExtn)o;
		 _log.debug("sendingMessage opcode:{} pdu:{}",msg.getOperationCode(),msg.getPdu());

		 if (msg.getOperationCode() == 60) {
			 try {
				 InitialDPSMSArg idpsms = (InitialDPSMSArg) msg.getPdu();
				 if( asn1TimeAndTimezone!=null) {
					 OctetString str = new OctetString(DatatypeConverter.parseHexBinary(asn1TimeAndTimezone));
					 idpsms.setTimeAndTimezone(str);
				 }
				 if( smsDataCoding!=null){
					 TPDataCodingScheme codingScheme = new TPDataCodingScheme();
					 codingScheme.setValue(DatatypeConverter.parseHexBinary(smsDataCoding));
					 idpsms.setTPDataCodingScheme(codingScheme);
				 }
				 if( smsProtocolIdentif!=null){
					 TPProtocolIdentifier val = new TPProtocolIdentifier();
					 val.setValue(DatatypeConverter.parseHexBinary(smsProtocolIdentif));
					 idpsms.setTPProtocolIdentifier(val);
				 }
				 if( smsShortMsgSecInfo!=null){
					 TPShortMessageSubmissionInfo val = new TPShortMessageSubmissionInfo();
					 val.setValue(DatatypeConverter.parseHexBinary(smsShortMsgSecInfo));
					 idpsms.setTPShortMessageSubmissionInfo(val);
				 }
				 if( smsValidityPeriod!=null){
					 TPValidityPeriod val = new TPValidityPeriod();
					 val.setValue(DatatypeConverter.parseHexBinary(smsValidityPeriod));
					 idpsms.setTPValidityPeriod(val);
				 }
			 } catch (Exception e) {
				 _log.error("Exception", e);
			 }
		 } else if (msg.getOperationCode() == 0) {
			 try {
				 Object pdu = msg.getPdu();
				 _log.debug("Catching sending InitialDP, sending params timeAndTimezone:{} locationInformation:{}",asn1TimeAndTimezone,asn1LocationInformation);
				 if(  pdu instanceof com.hp.opencall.camel.camel3.oc_camel3_pdus.InitialDPArg){
					 com.hp.opencall.camel.camel3.oc_camel3_pdus.InitialDPArg cap3IDP = (com.hp.opencall.camel.camel3.oc_camel3_pdus.InitialDPArg) pdu;
					 if( asn1TimeAndTimezone!=null) {
						 OctetString str = new OctetString(DatatypeConverter.parseHexBinary(asn1TimeAndTimezone));
						 cap3IDP.setTimeAndTimezone(str);
					 }
					 if( asn1LocationInformation!=null ){
						 Integer ageOfLocation = (Integer) asn1LocationInformation.get("ageOfLocation");
						 if( ageOfLocation!=null) {
							 AgeOfLocationInformation age = new AgeOfLocationInformation();
							 age.setValue(ageOfLocation);
							 cap3IDP.getLocationInformation().setAgeOfLocationInformation(age);
						 }
						 String saiCellID=(String) asn1LocationInformation.get("cellIdSAI");
						 if( saiCellID!=null){
							 CellGlobalIdOrServiceAreaIdOrLAI cellid = new CellGlobalIdOrServiceAreaIdOrLAI();
							 CellGlobalIdOrServiceAreaIdFixedLength sai = new CellGlobalIdOrServiceAreaIdFixedLength();
							 sai.setValue(DatatypeConverter.parseHexBinary(saiCellID));
							 cellid.setCellGlobalIdOrServiceAreaIdFixedLength(sai);
							 cap3IDP.getLocationInformation().setCellGlobalIdOrServiceAreaIdOrLAI(cellid);
						 }
						 Boolean saiPresent = (Boolean) asn1LocationInformation.get("saiPresent");
						 if( saiPresent!=null && saiPresent==true ){
							 Null nullSai=new Null();
							 cap3IDP.getLocationInformation().setSai_Present(nullSai);
						 }
					 }
				 } else if ( pdu instanceof com.hp.opencall.camel.camel2.cap_datatypes.InitialDPArg){
					 com.hp.opencall.camel.camel2.cap_datatypes.InitialDPArg cap3IDP = (com.hp.opencall.camel.camel2.cap_datatypes.InitialDPArg) pdu;
					 if( asn1TimeAndTimezone!=null) {
						 TimeAndTimezone tz = new TimeAndTimezone();
						 tz.setValue(DatatypeConverter.parseHexBinary(asn1TimeAndTimezone));
						 cap3IDP.setTimeAndTimezone(tz);
					 }
					 if( asn1LocationInformation!=null ){
						 Integer ageOfLocation = (Integer) asn1LocationInformation.get("ageOfLocation");
						 if( ageOfLocation!=null) {
							 com.hp.opencall.camel.camel2.map_ms_datatypes.AgeOfLocationInformation age = new com.hp.opencall.camel.camel2.map_ms_datatypes.AgeOfLocationInformation();
							 age.setValue(ageOfLocation);
							 cap3IDP.getLocationInformation().setAgeOfLocationInformation(age);
						 }
						 String cellId=(String) asn1LocationInformation.get("cellId");
						 if( cellId!=null){
							 com.hp.opencall.camel.camel2.map_commondatatypes.CellIdOrLAI cellIdOrLAI = new com.hp.opencall.camel.camel2.map_commondatatypes.CellIdOrLAI();
							 com.hp.opencall.camel.camel2.map_commondatatypes.CellIdFixedLength cellid = new com.hp.opencall.camel.camel2.map_commondatatypes.CellIdFixedLength();
							 cellid.setValue(DatatypeConverter.parseHexBinary(cellId));
							 cellIdOrLAI.setCellIdFixedLength(cellid);
							 cap3IDP.getLocationInformation().setCellIdOrLAI(cellIdOrLAI);
						 }

						 String cellIdLAI=(String) asn1LocationInformation.get("cellIdLAI");
						 if( cellIdLAI!=null){
							 com.hp.opencall.camel.camel2.map_commondatatypes.CellIdOrLAI cellIdOrLAI = new com.hp.opencall.camel.camel2.map_commondatatypes.CellIdOrLAI();
							 LAIFixedLength cellid = new LAIFixedLength();
							 cellid.setValue(DatatypeConverter.parseHexBinary(cellIdLAI));
							 cellIdOrLAI.setLaiFixedLength(cellid);
							 cap3IDP.getLocationInformation().setCellIdOrLAI(cellIdOrLAI);
						 }

					 }
				 } else if ( pdu instanceof com.hp.opencall.camel.camel1.cap_datatypes.InitialDPArg){
					 com.hp.opencall.camel.camel1.cap_datatypes.InitialDPArg cap1IDP = (com.hp.opencall.camel.camel1.cap_datatypes.InitialDPArg) pdu;
					 if( asn1TimeAndTimezone!=null) {
						 OctetString str = new OctetString(DatatypeConverter.parseHexBinary(asn1TimeAndTimezone));
						 //
					 }
					 if( asn1LocationInformation!=null ){
						 Integer ageOfLocation = (Integer) asn1LocationInformation.get("ageOfLocation");
						 if( ageOfLocation!=null) {
							 com.hp.opencall.camel.camel1.map_ms_datatypes.AgeOfLocationInformation age = new com.hp.opencall.camel.camel1.map_ms_datatypes.AgeOfLocationInformation ();
							 age.setValue(ageOfLocation);
							 cap1IDP.getLocationInformation().setAgeOfLocationInformation(age);
						 }
						 String saiCellID=(String) asn1LocationInformation.get("cellId");
						 if( saiCellID!=null){
							 CellIdOrLAI cellid = new CellIdOrLAI();
							 CellIdFixedLength sai = new CellIdFixedLength();
							 sai.setValue(DatatypeConverter.parseHexBinary(saiCellID));
							 cellid.setCellIdFixedLength(sai);
							 cap1IDP.getLocationInformation().setCellIdOrLAI(cellid);
						 }
						 String cellIdLAI=(String) asn1LocationInformation.get("cellIdLAI");
						 if( cellIdLAI!=null){
							 CellIdOrLAI laiCellId = new CellIdOrLAI();
							 com.hp.opencall.camel.camel1.map_commondatatypes.LAIFixedLength cellid = new com.hp.opencall.camel.camel1.map_commondatatypes.LAIFixedLength();
							 cellid.setValue(DatatypeConverter.parseHexBinary(cellIdLAI));
							 laiCellId.setLaiFixedLength(cellid);
							 cap1IDP.getLocationInformation().setCellIdOrLAI(laiCellId);
						 }
					 }
				 }
			 } catch (Exception e) {
				 _log.error("Exception",e);
			 }
		 } else if (msg.getOperationCode() == 20) {
			 Object pdu = msg.getPdu();
			 _log.debug("Handling asn1connect:{}", asn1Connect);
			 if (asn1Connect != null) {
				 String originalCdPa = (String) asn1Connect.get("originalCalledPartyID");
				 String redirPa = (String) asn1Connect.get("redirectingPartyID");
				 String redirInfo = (String) asn1Connect.get("redirectionInformation");
				 String destPA = (String) asn1Connect.get("destination");
				 if (pdu instanceof ConnectArg) {
					 ConnectArg con = (ConnectArg) pdu;

					 if (originalCdPa != null) {
						 OctetString buf = new OctetString();
						 buf.setValue(DatatypeConverter.parseHexBinary(originalCdPa));
						 con.setOriginalCalledPartyID(buf);
					 }
					 if (redirPa != null) {
						 OctetString buf = new OctetString();
						 buf.setValue(DatatypeConverter.parseHexBinary(redirPa));
						 con.setRedirectingPartyID(buf);
					 }
					 if (redirInfo != null) {
						 RedirectionInformation redirectionInformation = new RedirectionInformation();
						 redirectionInformation.setValue(DatatypeConverter.parseHexBinary(redirInfo));
						 con.setRedirectionInformation(redirectionInformation);
					 }
					 if( destPA!=null){
						 _log.debug("set new destination:{}",destPA);
						 OctetString dest = new OctetString();
						 dest.setValue(DatatypeConverter.parseHexBinary(destPA));
						 con.getDestinationRoutingAddress().set(dest,0);
					 }
				 } else if (pdu instanceof com.hp.opencall.camel.camel2.cap_datatypes.ConnectArg) {
					 com.hp.opencall.camel.camel2.cap_datatypes.ConnectArg con = (com.hp.opencall.camel.camel2.cap_datatypes.ConnectArg) pdu;
					 if (originalCdPa != null) {
						 OriginalCalledPartyID originalCalledPartyID = new OriginalCalledPartyID();
						 originalCalledPartyID.setValue(DatatypeConverter.parseHexBinary(originalCdPa));
						 con.setOriginalCalledPartyID(originalCalledPartyID);
					 }
					 if (redirPa != null) {
						 RedirectingPartyID redirectingPartyID = new RedirectingPartyID();
						 redirectingPartyID.setValue(DatatypeConverter.parseHexBinary(redirPa));
						 con.setRedirectingPartyID(redirectingPartyID);
					 }
					 if (redirInfo != null) {
						 RedirectionInformation redirectionInformation = new RedirectionInformation();
						 redirectionInformation.setValue(DatatypeConverter.parseHexBinary(redirInfo));
						 con.setRedirectionInformation(redirectionInformation);
					 }
					 if( destPA!=null){
						 CalledPartyNumber cdpa = new CalledPartyNumber();
						 cdpa.setValue(DatatypeConverter.parseHexBinary(destPA));
						 con.getDestinationRoutingAddress().set(cdpa,0);
					 }

				 } else if (pdu instanceof com.hp.opencall.camel.camel1.cap_datatypes.ConnectArg) {
					 com.hp.opencall.camel.camel1.cap_datatypes.ConnectArg con = (com.hp.opencall.camel.camel1.cap_datatypes.ConnectArg) pdu;
					 if (originalCdPa != null) {
						 com.hp.opencall.camel.camel1.cap_datatypes.OriginalCalledPartyID originalCalledPartyID = new com.hp.opencall.camel.camel1.cap_datatypes.OriginalCalledPartyID();
						 originalCalledPartyID.setValue(DatatypeConverter.parseHexBinary(originalCdPa));
						 con.setOriginalCalledPartyID(originalCalledPartyID);
					 }
					 if (redirPa != null) {
						 com.hp.opencall.camel.camel1.cap_datatypes.RedirectingPartyID redirectingPartyID = new com.hp.opencall.camel.camel1.cap_datatypes.RedirectingPartyID();
						 redirectingPartyID.setValue(DatatypeConverter.parseHexBinary(redirPa));
						 con.setRedirectingPartyID(redirectingPartyID);
					 }
					 if (redirInfo != null) {
						 RedirectionInformation redirectionInformation = new RedirectionInformation();
						 redirectionInformation.setValue(DatatypeConverter.parseHexBinary(redirInfo));
						 con.setRedirectionInformation(redirectionInformation);
					 }
					 if( destPA!=null){
						 com.hp.opencall.camel.camel1.cap_datatypes.CalledPartyNumber cdpa = new com.hp.opencall.camel.camel1.cap_datatypes.CalledPartyNumber();
						 cdpa.setValue(DatatypeConverter.parseHexBinary(destPA));
						 con.getDestinationRoutingAddress().set(cdpa,0);
					 }

				 }
			 }
			 //reset asn1 connect
			 asn1Connect = null;
		 } else if (msg.getOperationCode()==66) {
			 Object pdu = msg.getPdu();
			 if( pdu instanceof ReleaseSMSArg) {
				 ReleaseSMSArg relSMS = (ReleaseSMSArg) pdu;
			 }
		 } else if (msg.getOperationCode() == 36) {

		 } else if (msg.getOperationCode() == 66) {
		 } else if (msg.getOperationCode() == 34) {
			 Object pdu = msg.getPdu();
			 _log.debug("Handling fci");
			 if( pdu instanceof FurnishChargingInformationArg){
			 }
		 } else if (msg.getOperationCode() == 24) {
			 _log.debug("asn1 ERB:{}",asn1Erb);
			 try {
				 Object pdu = msg.getPdu();
				 String cause=null;
				 Integer erb=null;
				 Integer sendLegId=null;
				 Integer recvLegId=null;
				 Integer miscMessageType = null;
				 Boolean handleErb=false;
				 if (asn1Erb!=null){
					 cause = (String)asn1Erb.get("cause");
					 erb = (Integer) asn1Erb.get("erb");
					 if( asn1Erb.containsKey("sendLegId")){
						 sendLegId = (Integer) asn1Erb.get("sendLegId");
					 } else if (asn1Erb.containsKey("recvLegId")){
						 recvLegId = (Integer) asn1Erb.get("recvLegId");
					 }
					 if( asn1Erb.containsKey("miscMessageType")){
						 miscMessageType=(Integer) asn1Erb.get("miscMessageType");
 					 }
					 handleErb=true;
				 }
				 _log.debug("Intercepting ERB set asn1 fields:{} handleERB:{}",asn1Erb,handleErb);

				 asn1Erb=null;
				 if( handleErb ) {

					 if (pdu instanceof EventReportBCSMArg) {
						 _log.debug("Fill CAMEL2 ...");
						 EventReportBCSMArg erb2 = (EventReportBCSMArg) pdu;
						 //camel2
						 if( cause!=null) {
							 OctetString buffer = new OctetString();
							 buffer.setValue(DatatypeConverter.parseHexBinary(cause));

							 com.hp.opencall.camel.camel2.cap_datatypes.Cause cause1 = new com.hp.opencall.camel.camel2.cap_datatypes.Cause();
							 cause1.setValue(buffer.byteArrayValue());

							 EventSpecificInformationBCSM eventSpecificInformationBCSM = new EventSpecificInformationBCSM();
							 if( erb2.getEventTypeBCSM().longValue() == RouteSelectFailure){
								 EventSpecificInformationBCSM.RouteSelectFailureSpecificInfo rsf = new EventSpecificInformationBCSM.RouteSelectFailureSpecificInfo();
								 rsf.setFailureCause(cause1);
								 eventSpecificInformationBCSM.setRouteSelectFailureSpecificInfo(rsf);
								 erb2.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb2.getEventTypeBCSM().longValue() == OCalledPartyBusy){
								 EventSpecificInformationBCSM.OCalledPartyBusySpecificInfo oBusy =  new EventSpecificInformationBCSM.OCalledPartyBusySpecificInfo();
								 oBusy.setBusyCause(cause1);
								 eventSpecificInformationBCSM.setOCalledPartyBusySpecificInfo(oBusy);
								 erb2.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb2.getEventTypeBCSM().longValue() == ODisconnect){
								 EventSpecificInformationBCSM.ODisconnectSpecificInfo oDisconnect = new EventSpecificInformationBCSM.ODisconnectSpecificInfo();
								 oDisconnect.setReleaseCause(cause1);
								 eventSpecificInformationBCSM.setODisconnectSpecificInfo(oDisconnect);
								 erb2.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb2.getEventTypeBCSM().longValue()== TCalledPartyBusy ) {
								 EventSpecificInformationBCSM.TBusySpecificInfo tBusy = new EventSpecificInformationBCSM.TBusySpecificInfo();
								 tBusy.setBusyCause(cause1);
								 eventSpecificInformationBCSM.setTBusySpecificInfo(tBusy);
								 erb2.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb2.getEventTypeBCSM().longValue()== TDisconnect ) {
								 EventSpecificInformationBCSM.TDisconnectSpecificInfo tDisconnect = new EventSpecificInformationBCSM.TDisconnectSpecificInfo();
								 tDisconnect.setReleaseCause(cause1);
								 eventSpecificInformationBCSM.setTDisconnectSpecificInfo(tDisconnect);
								 erb2.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 }

						 }

						 com.hp.opencall.camel.common.cs1_datatypes.LegType type = new com.hp.opencall.camel.common.cs1_datatypes.LegType();
						 byte[] tmp = new byte[1];
						 tmp[0]=2;
						 type.setValue(tmp);

						 if( recvLegId!=null ){
							 tmp[0]=recvLegId.byteValue();
							 type.setValue(tmp);
							 com.hp.opencall.camel.camel2.cap_datatypes.ReceivingSideID recv = new com.hp.opencall.camel.camel2.cap_datatypes.ReceivingSideID();
							 recv.setReceivingSideID(type);
							 erb2.setLegID(recv);
						 }
						 if( miscMessageType!=null ){
							 com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo miscCallInfo = new com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo();
							 if( miscMessageType==1) {
								 miscCallInfo.setMessageType(com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo.MessageType.notification);
							 } else {
								 miscCallInfo.setMessageType(com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo.MessageType.request);
							 }
							 erb2.setMiscCallInfo(miscCallInfo);
						 }

						 //rsf


					 } else if (pdu instanceof com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg) {
						 _log.debug("Fill CAMEL3 ...");
						 //camel3
						 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg erb3 = (com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg) pdu;
						 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM eventSpecificInformationBCSM = new com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM();


						 com.hp.opencall.camel.common.cs1_datatypes.LegType type = new com.hp.opencall.camel.common.cs1_datatypes.LegType();
						 byte[] tmp = new byte[1];
						 tmp[0]=2;
						 type.setValue(tmp);

						 if( recvLegId!=null ){
							 tmp[0]=recvLegId.byteValue();
							 type.setValue(tmp);
							 ReceivingSideID recv = new ReceivingSideID();
							 recv.setReceivingSideID(type);
							 erb3.setLegID(recv);
						 }
						 if( miscMessageType!=null ){
							 MiscCallInfo miscCallInfo = new MiscCallInfo();
							 if( miscMessageType==1) {
								 miscCallInfo.setMessageType(MiscCallInfo.MessageType.notification);
							 } else {
								 miscCallInfo.setMessageType(MiscCallInfo.MessageType.request);
							 }
							 erb3.setMiscCallInfo(miscCallInfo);
						 }


						 if( cause!=null) {
							 OctetString buffer = new OctetString();
							 buffer.setValue(DatatypeConverter.parseHexBinary(cause));

							 //rsf
							 if (erb3.getEventTypeBCSM().longValue() == RouteSelectFailure) {
								 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.RouteSelectFailureSpecificInfo rsf = new com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.RouteSelectFailureSpecificInfo();
								 rsf.setFailureCause(buffer);
								 eventSpecificInformationBCSM.setRouteSelectFailureSpecificInfo(rsf);
								 erb3.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb3.getEventTypeBCSM().longValue() == OCalledPartyBusy) {
								 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.OCalledPartyBusySpecificInfo oBusy = new com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.OCalledPartyBusySpecificInfo();
								 oBusy.setBusyCause(buffer);
								 eventSpecificInformationBCSM.setOCalledPartyBusySpecificInfo(oBusy);
								 erb3.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb3.getEventTypeBCSM().longValue() == ODisconnect) {
								 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.ODisconnectSpecificInfo oDisconnect = new com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.ODisconnectSpecificInfo();
								 oDisconnect.setReleaseCause(buffer);
								 eventSpecificInformationBCSM.setODisconnectSpecificInfo(oDisconnect);
								 erb3.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb3.getEventTypeBCSM().longValue() == TCalledPartyBusy) {
								 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.TBusySpecificInfo tBusy = new com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.TBusySpecificInfo();
								 tBusy.setBusyCause(buffer);
								 eventSpecificInformationBCSM.setTBusySpecificInfo(tBusy);
								 erb3.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 } else if (erb3.getEventTypeBCSM().longValue() == TDisconnect) {
								 com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.TDisconnectSpecificInfo tDisconnect = new com.hp.opencall.camel.camel3.oc_camel3_pdus.EventReportBCSMArg.EventSpecificInformationBCSM.TDisconnectSpecificInfo();
								 tDisconnect.setReleaseCause(buffer);
								 eventSpecificInformationBCSM.setTDisconnectSpecificInfo(tDisconnect);
								 erb3.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 }
						 }
					 }  else if (pdu instanceof com.hp.opencall.camel.camel1.cap_datatypes.EventReportBCSMArg ) {
						 _log.debug("Fill CAMEL1 ... miscCalInfo:{}",miscMessageType);
						 //camel1

						 com.hp.opencall.camel.camel1.cap_datatypes.EventReportBCSMArg erb1 = (com.hp.opencall.camel.camel1.cap_datatypes.EventReportBCSMArg) pdu;
						 if( cause!=null) {
							 OctetString buffer = new OctetString();
							 buffer.setValue(DatatypeConverter.parseHexBinary(cause));

							 com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM eventSpecificInformationBCSM = new com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM();
							 com.hp.opencall.camel.camel1.cap_datatypes.Cause cause2 = new com.hp.opencall.camel.camel1.cap_datatypes.Cause();
							 cause2.setValue(buffer.byteArrayValue());
							 if (erb1.getEventTypeBCSM().longValue() == ODisconnect){
								 com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM.ODisconnectSpecificInfo oDisconnect = new com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM.ODisconnectSpecificInfo();
								 oDisconnect.setReleaseCause(cause2);
								 eventSpecificInformationBCSM.setODisconnectSpecificInfo(oDisconnect);
								 erb1.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);

							 } else if (erb1.getEventTypeBCSM().longValue()== TDisconnect ) {
								 com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM.TDisconnectSpecificInfo tDisconnect = new com.hp.opencall.camel.camel1.cap_datatypes.EventSpecificInformationBCSM.TDisconnectSpecificInfo();
								 tDisconnect.setReleaseCause(cause2);
								 eventSpecificInformationBCSM.setTDisconnectSpecificInfo(tDisconnect);
								 erb1.setEventSpecificInformationBCSM(eventSpecificInformationBCSM);
							 }

						 }

						 LegID legid = new LegID();
						 LegType type = new LegType();
						 byte[] tmp = new byte[1];
						 tmp[0]=1;
						 type.setValue(tmp);
						 legid.setReceivingSideID(type);

						 if( recvLegId!=null ){
							 tmp[0]=recvLegId.byteValue();
							 type.setValue(tmp);
							 legid.setReceivingSideID(type);
						 } else if (sendLegId!=null ) {
							 tmp[0]=sendLegId.byteValue();
							 type.setValue(tmp);
							 legid.setReceivingSideID(type);
						 }

						 erb1.setLegID(legid);

						 if( miscMessageType!=null ){

							 com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo miscCallInfo = new com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo();
							 if( miscMessageType==1) {
								 miscCallInfo.setMessageType(com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo.MessageType.notification);
							 } else {
								 miscCallInfo.setMessageType(com.hp.opencall.camel.common.cs1_datatypes.MiscCallInfo.MessageType.request);
							 }
							 erb1.setMiscCallInfo(miscCallInfo);
						 }

					 }

				 }
			 } catch (Exception e){
				 _log.error("Exception",e);
			 }
		 }

	 }

	 public void setJsonCall(JsonSCIFRequest value){
		this.jsonCall=value;
		this.callId=this.jsonCall.getCallId();
	 }

	 public List<InCallMonitorEvent> getVssfMonitorEvents(){
		return this.vssfMonitorEvents;
	 }

	 public List<InSmsMonitorEvent> getVssfSMSMonitorEvents(){
		 return this.vssfSMSMonitorEvents;
	 }

	 public InCallChargingEvent getVssfChargingEvent(){
		 return this.vssfChargingEvent;
	 }

	 public InCallDirectMediaEvent getVssfMediaEvent() {
		 return this.vssfDirectMediaEvent;
	 }

	 public InCallMediaPAEvent getVssfMediaPAEvent() {
		return this.vssfMediaPAEvent;
	 }

	 public InCallMediaPACEvent getVssfMediaPACEvent() {
		 return this.vssfMediaPACEvent;
	 }

	 public InCallDiscMediaEvent getVssfdiscMediaEvent(){
		return this.vssfdiscMediaEvent;
	 }


	 public Map<String,InPollEvent> getVssfPendingEvents() {
		return this.vssfPendingEvents;
	 }

	 private void getValidQueue(){
		_log.debug("Existing queue:{} cache queue:{} size:{}",this.egressQueue,ConsumersCache.get().getConsumer(this.egressQueue), ConsumersCache.get().size());

		if((Instant.now().toEpochMilli() -this.lastQueueSetTime)>12000 &&
				!this.egressQueue.equalsIgnoreCase(Parameters.get().getQueueEgress()) &&
				ConsumersCache.get().getConsumer(this.egressQueue)==null){
			this.egressQueue=Parameters.get().getQueueEgress();
		}
		 _log.debug("Valid queue:{}",this.egressQueue);
	 }

	 public CallLeg getSrfLeg () {
		return this.srfLeg;
	 }

	 public ApplicationSession getAppSession(){
		return this.appSession;
	 }
	 public void setApplicationSession(final ApplicationSession value){
		if(value==null){
			return;
		}
		if( this.appSession==null ){
			this.appSession=value;
		}
	 }

	 public boolean acceptMediaDisconnect(){
		 _log.debug("Checking to accept media disconnect:{}",lastINPollEventList);
		 if( lastINPollEventList!=null){
			 for(InPollEvent event : lastINPollEventList) {
				 if( event.getType()==InCallDiscMediaEvent.IN_CALL_DISC_MEDIA_POLL_EVENT_TYPE){
					 return  true;
				 }
			 }
		 }
		 return false;
	 }
 }


