package com.eitg.quasar.occp.cap.servicelogic;

import com.hp.opencall.imscapi.annotation.ImportFeature;
import com.hp.opencall.imscapi.annotation.ImsApplication;

@ImsApplication(name = "CamelService", imslets = { "CamelServiceImslet" },
importedFeatures = {
        @ImportFeature(name = "ImscScifTjb-${CALL_PROVIDER_TECHNO}", interface_name = "com.hp.opencall.ngin.scif.impl.imsc.ImscScifFeatureSessionIntf")}
)
public class CapImsapp {
//        @ImportFeature(name = "ImscScifTjb-${IN_CALL_PROVIDER_TECHNO}", interface_name = "com.hp.opencall.ngin.scif.impl.imsc.ImscScifFeatureSessionIntf")},
        //@ImportFeature(name = "ImscScifTjb-${DIAMETER_CALL_PROVIDER_TECHNO}", interface_name = "com.hp.opencall.ngin.scif.impl.imsc.ImscScifFeatureSessionIntf") },
}
