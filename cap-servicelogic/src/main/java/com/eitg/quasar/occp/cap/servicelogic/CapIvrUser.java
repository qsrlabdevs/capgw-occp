package com.eitg.quasar.occp.cap.servicelogic;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.businesslogic.Parameters;
import com.eitg.quasar.occp.utils.JsonSCIFRequest;
import com.eitg.quasar.occp.utils.JsonSCIFResponse;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.resources.IvrCallLeg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.resources.IvrCallLeg.IvrUser;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class CapIvrUser implements IvrUser, MessageInterceptor {
	private static final Logger _log = LoggerFactory.getLogger(CapIvrUser.class.getName());

	private WeakReference<JsonSCIFRequest> jsonCall ;
	private WeakReference<JsonSession> jsonSession;
	private WeakReference<IvrCallLeg> ivrLeg;

	private String egressQueue=Parameters.get().getQueueEgress();

	public CapIvrUser(JsonSCIFRequest jsonCall, JsonSession jsonSession, IvrCallLeg leg, String queue ){
		_log.debug("Initialize CapIvrUser leg:{}",leg);
		this.jsonCall =  new WeakReference<>( jsonCall );
		this.jsonSession = new WeakReference<>( jsonSession );
		this.ivrLeg = new WeakReference<>(leg);
		this.egressQueue=queue;

		Call call =  this.jsonCall.get().getCall();

		CommonParameterSet commonParameterSet=call.getParameterSet(CommonParameterSet.class);
		commonParameterSet.setMessageInterceptor(this);

	}

	@Override
	public void collectComplete(Cause cause, String collectedDigits) {
		_log.debug("Collect complete cause:{}",cause);
		try {
			Map<String, Object> eventData = new HashMap<>();
			eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.COLLECTCOMPLETE);
			eventData.put(JsonSCIFRequest.EventId, jsonCall.get().getEventId());
			eventData.put(JsonSCIFResponse.CALL, jsonCall.get().getCall());
			eventData.put(JsonSCIFResponse.SESSION, jsonSession.get());
			eventData.put(JsonSCIFResponse.LEG, this.ivrLeg.get());

			Map<String, Object> rawMessage = jsonCall.get().getJsonCollectComplete(cause,collectedDigits);
			RedisProcessorThread.parkEvent(jsonCall.get().getCallId(), eventData);
			boolean status =  RedisPool.get().sendToQueue(rawMessage, Parameters.get().getQueueEgress());
		} finally {
			jsonCall.get().incrementEventId();
		}


	}

	@Override
	public void playComplete(Cause cause) {
		_log.debug("Play complete cause:{}",cause);
		try {
			Map<String, Object> eventData = new HashMap<>();
			eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.PLAYCOMPLETE);
			eventData.put(JsonSCIFRequest.EventId, jsonCall.get().getEventId());
			eventData.put(JsonSCIFResponse.CALL, jsonCall.get().getCall());
			eventData.put(JsonSCIFResponse.SESSION, jsonSession.get());
			eventData.put(JsonSCIFResponse.LEG, this.ivrLeg.get());

			Map<String, Object> rawMessage = jsonCall.get().getJsonPlayComplete(cause);
			RedisProcessorThread.parkEvent(jsonCall.get().getCallId(), eventData);
			boolean status =  RedisPool.get().sendToQueue(rawMessage, Parameters.get().getQueueEgress());
		} finally {
			jsonCall.get().incrementEventId();
		}
	}

	@Override
	public void messageReceived(Object o, CallLeg callLeg) {
		InMessageInterceptorExtn msg = (InMessageInterceptorExtn)o;

		_log.debug("IvrUser messageReceived opcode:{} pdu:{}",msg.getOperationCode(),msg.getPdu());
	}

	@Override
	public void sendingMessage(Object o, CallLeg callLeg) throws ScifException {
		InMessageInterceptorExtn msg = (InMessageInterceptorExtn)o;

		_log.debug("IvrUser sendingMessage opcode:{} pdu:{}",msg.getOperationCode(),msg.getPdu());
	}
}
