package com.eitg.quasar.occp.cap.servicelogic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import javax.management.NotCompliantMBeanException;

import com.eitg.quasar.nexus.middleware.utils.Commons;
import com.eitg.quasar.occp.cap.metrics.MetricsServer;
import com.eitg.quasar.occp.overload.OverloadProtection;
import com.eitg.quasar.occp.utils.ConsumersCache;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.cap.businesslogic.Parameters;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import com.hp.opencall.imscapi.LifecycleException;
import com.hp.opencall.imscapi.annotation.ImsletDescription;
import com.hp.opencall.imscapi.imslet.ImsletException;
import com.hp.opencall.ngin.scif.CallProvider;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.ScifFactory;
import com.hp.opencall.ngin.scif.imsc.ScifImslet;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerFactory;
import com.hp.opencall.ngin.timer.TimerListener;

@ImsletDescription(
        name="CamelServiceImslet"
)
public class CapServiceImslet extends ScifImslet {
	private static final Logger _log = LoggerFactory.getLogger(CapServiceImslet.class.getName());
	

	private static final Long CALL_PROVIDER_RETRY_COUNT = 5L;
	private static final Long CALL_PROVIDER_RETRY_TIMEOUT = 60 * 1000L;
	private static final Long DEFAULT_TIMEOUT = 5 * 60 * 1000L;
	private static final String SERVICE_CALL_PROVIDER_TECHNO_PARAM_NAME = "NgIn.Srv.CAMEL.CALL_PROVIDER_TECHNO";
	private static final String SERVICE_CALL_PROVIDER_NAME_PARAM_NAME = "NgIn.Srv.CAMEL.CALL_PROVIDER_NAME";
	private static final String SERVICE_VSSFCALL_PROVIDER_NAME_PARAM_NAME = "NgIn.Srv.CAMEL.VSSF_CALL_PROVIDER_NAME";



	public static volatile String ReleaseVersion = "v1_1_1";
	
	/**
	 * Represents the serial UID
	 */
	protected static final long serialVersionUID = 1L;


	boolean shouldStop=false;
	/**
	 * Represents the service's call user factory
	 */
	protected CapUserFactory userFactory = null;
	/**
	 * Represents the tracer
	 */
	
	private static CallProvider callProvider = null;
	private static String callProviderTechno="";
	private static String vssfCallProviderName="";
	private Object beanWrapper = null;

	public static final String NAME = "CamelService";
	private static boolean loaded=false;

	public static int numberOfCalls=0;
	private List<RedisProcessorThread> redisProcessors;
	
	public CapServiceImslet() {
		// call super class's constructor
		super();
		
		_log.debug("->CapServiceImslet() ");
	}

	public void init() {
		// call superclass's init()
		
		try {
			super.init();
		} catch (ImsletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_log.debug("->init()");
	}

	public void unlocked()   {
		// call superclasse's unlocked()

		try {
			super.unlocked();
		} catch (ImsletException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		_log.debug("->unlocked()");

		Long timeout = DEFAULT_TIMEOUT;

		_log.debug(CapUserFactory.NAME + " uses overload timeout: " + timeout);

		// add our bean
		String imsname = String.format("%s_%d", getImsletContext().getServiceCmpHostname(),
				getImsletContext().getServiceCmpVmId());

		userFactory = new CapUserFactory();

		_log.debug("ScifFactory instance: " + ScifFactory.instance());
		// get the service call provider (should be CAP2) and register
		// our service factory to it
		_log.debug("Get parameter:" + SERVICE_CALL_PROVIDER_TECHNO_PARAM_NAME);
		callProviderTechno = getImsletContext().getInitParameter(SERVICE_CALL_PROVIDER_TECHNO_PARAM_NAME);
		_log.debug(userFactory.getName() + " uses (" + callProviderTechno + ") call provider technology");
		_log.debug("Get parameter:" + SERVICE_CALL_PROVIDER_NAME_PARAM_NAME);
		String callProviderName = getImsletContext().getInitParameter(SERVICE_CALL_PROVIDER_NAME_PARAM_NAME);

		vssfCallProviderName = getImsletContext().getInitParameter(SERVICE_VSSFCALL_PROVIDER_NAME_PARAM_NAME);
		_log.debug(userFactory.getName() + " uses (" + callProviderName + ") call provider name vssfCallProvider:"+vssfCallProviderName);

		Parameters.initializeInstance(this);
		Parameters.setImsName(imsname);

		Long callProviderRetries = CALL_PROVIDER_RETRY_COUNT;
		_log.debug("Fetching call provider ...");


		while (callProviderRetries-- > 0) {
			try {
				callProvider = ScifFactory.instance().getCallProvider(callProviderTechno, callProviderName);
				if (callProvider != null) {
					_log.debug("CallProvider:" + callProvider);
					callProvider.addCallUserFactory(userFactory, null);
					break;
				} else {
					_log.debug("CallProvider: null");
				}
			} catch (ScifException e) {
				_log.error("Cannot obtain call provider, will retry", e);
			}

			try {
				Thread.sleep(CALL_PROVIDER_RETRY_TIMEOUT);
			} catch (InterruptedException e) {
				// ignore
			}
		}

		_log.debug("Initializing redis ...");
//		RedisPool.get().initialize(Parameters.get().getRedisUrl(), Parameters.get().getRedisPoolSize(), 1000);
		RedisPool.get().initialize(Parameters.get().getRedisUrl());
		_log.debug("Initializing redis ... {}",RedisPool.get().getDB());

		Map<String,Object> mainConfig = (Map<String, Object>) Commons.readConfigJsonFromRedis(Parameters.get().getRedisUrl());
		Map<String,Object> tasgwConf = (Map<String,Object>)mainConfig.get("TASGw");

		_log.debug("Main config:{}",mainConfig);
		Integer numberOfThreads=1;

		if( tasgwConf!=null && tasgwConf.containsKey("nb-threads")){
			numberOfThreads = (Integer) tasgwConf.get("nb-threads");
		}


		// add our bean
		//TODO - add Monitor Bean
		// startCountersLoggerThread();
		
		
		redisProcessors = new ArrayList<>();
		RedisProcessorThread.initializeRxPool(numberOfThreads);

		for(int i=0;i<numberOfThreads;i++) {
			RedisProcessorThread thread = new RedisProcessorThread(Parameters.getImsName());
			redisProcessors.add(thread);
			thread.start();
		}


		{
			if( Parameters.get().getQueueIngress()!=null) {
				RedisProcessorThread threadGenericIngress = new RedisProcessorThread(Parameters.get().getQueueIngress());
				redisProcessors.add(threadGenericIngress);
				threadGenericIngress.start();
			}
		}

		String svc = CapUserFactory.NAME;

		try {
			ScifFactory.instance().registerUiApplication("XML-FILE", svc, this);
		} catch (ScifException e) {
			e.printStackTrace();
		}


		Map<String,Object> overload = null;
		if( tasgwConf!=null ){
			overload = (Map<String,Object>)tasgwConf.get("overload");
			if( overload!=null ) {
				Integer maxExecTime = (Integer) overload.get("max-execution-time");
				Integer sleepTime = (Integer) overload.get("sleep-time");
				Integer maxQueueSize = (Integer) overload.get("queue-max-length");

				OverloadProtection.setMaxExecutionTime(maxExecTime);
				OverloadProtection.setSleepTime(sleepTime);
				OverloadProtection.setQueueMaxLength(maxQueueSize);
			}
		}

		_log.debug("Starting overload protection ...");
		try {
			OverloadProtection ovl = new OverloadProtection();
			ovl.start();
		} catch (Exception e){
			_log.error("Exception",e);
		}


		_log.debug("Starting metrics server ...:{}",Parameters.get().getHttpPort());
		MetricsServer.get().start();

		_log.debug("Initialize consumers cache ...");
		ConsumersCache.get();

		_log.debug("<-unlocked()");
	}

	public void destroy() {
		_log.debug("->destroy()");
		// stop the CDR writer thread
		shouldStop=true;
		super.destroy();
	}

	public boolean isShouldStop() {
		return shouldStop;
	}
	public Timer createTimer(TimerListener timerListener, long delay) {
		_log.debug("->createTimer()");
		return TimerFactory.instance().createTimer(getScifImsletApplicationSession(), timerListener, delay, null);
	}

	public static CapCallUser createCall( ){
		_log.debug("Create call ...");
		Call vssfCall=null;
		CapCallUser capCallUser=null;
		try {
			capCallUser  = new CapCallUser();
			CallProvider vssfCallProvider = null;
			String vssfTech = "CS";
			_log.debug("Get call provider:{} technology:{}",vssfCallProviderName,callProviderTechno);
			vssfCallProvider = ScifFactory.instance().getCallProvider(callProviderTechno, vssfCallProviderName);
			_log.debug("vssfCall provider:{}",vssfCallProvider);

			vssfCall = vssfCallProvider.createCall(null, capCallUser.getClass().getName(), capCallUser, callProviderTechno);

			//enable message interceptor
			CommonParameterSet commonParameterSet=vssfCall.getParameterSet(CommonParameterSet.class);
			commonParameterSet.setMessageInterceptor(capCallUser);


			//vssfCall = callProvider.createCall(appSession, capCallUser.getClass().getName(), capCallUser, callProviderTechno);

			capCallUser.setCall(vssfCall);
		} catch (Exception e){
			_log.error("Exception",e);
		}
		return capCallUser;
	}
}
