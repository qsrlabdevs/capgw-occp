package com.eitg.quasar.occp.cap.servicelogic;

import com.hp.opencall.ngin.scif.*;

public class CapVssfCallUser implements CallUser {
    @Override
    public void callStart(Cause cause, CallLeg callLeg, Contact contact) {

    }

    @Override
    public void callAnswered(Cause cause, CallLeg callLeg) {

    }

    @Override
    public void callEarlyAnswered(CallLeg callLeg) {

    }

    @Override
    public void callPoll(ScifEvent scifEvent) {

    }

    @Override
    public void callEnd(Cause cause) {

    }
}
