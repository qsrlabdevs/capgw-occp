package com.eitg.quasar.occp.overload;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.eitg.quasar.occp.cap.businesslogic.Parameters;
import com.eitg.quasar.occp.cap.metrics.MetricsServer;
import com.eitg.quasar.occp.cap.metrics.ValueGauge;
import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import com.eitg.quasar.occp.utils.ConsumersCache;
import com.eitg.quasar.occp.utils.RedisPool;
import com.eitg.quasar.occp.utils.RedisProcessorThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import redis.clients.jedis.exceptions.JedisException;

import java.util.Map;
import java.util.Set;


public class OverloadProtection extends Thread {
    private static Logger log = LoggerFactory.getLogger(OverloadProtection.class);


//    private MetricRegistry.MetricSupplier<Gauge> metricSupplier = new MetricRegistry.MetricSupplier<Gauge>() {
//        @Override
//        public Gauge newMetric() {
//            return new ValueGauge();
//        }
//    };


    public static boolean inOverload=false;
    public static boolean inDegraded=false;
    public static int queueMaxLength=10;
    public static long currentLength=0;
    private static int sleepTime=1000;

    private static int maxExecutionTime=1000;

    private static int conseqRedisFailures=0;
    private static int maxNoResponses=100;
    private static int maxResetNoResponsesCounter=20;
    private static int maxContinuousErrors=10;



    public static void setSleepTime(int value){
        sleepTime=value;
    }
    public static void setQueueMaxLength(int value){
        queueMaxLength=value;
    }
    public static void setMaxExecutionTime(int value){
        maxExecutionTime=value;
    }
    public static void setMaxNoResponses(int value) { maxNoResponses=value; }
    public static void setMaxResetNoResponsesCounter(int value) { maxResetNoResponsesCounter=value; }
    public static void setMaxContinuousErrors(int value) { maxContinuousErrors=value; }

    public OverloadProtection(){
        this.setName("OverloadProtection");
    }


    @Override
    public void run(){
        int noResponsesCounter=0;
        int continuousNoResponse=0;
        while(true){
            try {
                try {
                    log.debug("Checking queue length , sleep time:{}",sleepTime);
                    String counterSize = "tas.sessions.calls";
//                    ValueGauge gauge = (ValueGauge) MetricsServer.get().getMetrics().gauge(counterSize, metricSupplier);
//                    gauge.setValue(CapCallUser.calls.size());
//
//                    counterSize = "tas.sessions.parkedevents";
//                    gauge = (ValueGauge) MetricsServer.get().getMetrics().gauge(counterSize, metricSupplier);
//                    gauge.setValue(RedisProcessorThread.parkedEvents.size());

                    log.info("Map size of pending calls:{} parked events:{} queues size:{}",
                            CapCallUser.calls.size(),
                            RedisProcessorThread.parkedEvents.size(),
                            ConsumersCache.get().size());
                    try (Jedis resource = RedisPool.get().getDB().getResource()) {
                        Long queueLength = resource.llen(Parameters.get().getQueueEgress());
                        Long queueLengthIngress = resource.llen(Parameters.getImsName());
                        currentLength = queueLength;
                        log.info("Queue egress:{} ingress:{} maxLength:{} ", queueLength,queueLengthIngress, queueMaxLength);
                        if (queueLength >= queueMaxLength ||
                                queueLengthIngress >=queueMaxLength) {
                            log.error("Enter to overload, length egress:{} ingress:{} max length:{}", queueLength,queueLengthIngress, queueMaxLength);
                            inOverload = true;
                        } else {
                            inOverload = false;
                        }
                        inDegraded = false;
                    }

                    MetricsServer.get().getMetrics().histogram("capgw.overload.queueLength").update(currentLength);
                    conseqRedisFailures = 0;
                } catch (JedisConnectionException je) {
                    conseqRedisFailures++;

                    if (conseqRedisFailures > 5) {
                        log.error("JedisConnectionException - Shutdown application");
                        //System.exit(-1);
                    }

                    log.error("JedisConnectionException", je);
                    inDegraded = true;
                } catch (JedisException e) {
                    conseqRedisFailures++;
                    if (conseqRedisFailures > 10) {
                        //log.error("JedisException - Shutdown application");
                        //System.exit(-1);
                    }
                    inDegraded = true;
                    log.error("Exception", e);
                }


                try {
                    //check execution time
                    log.info("Execution time max:{} current:{}", maxExecutionTime, RedisProcessorThread.getExecutiontimer().getSnapshot().get75thPercentile());
                    if (RedisProcessorThread.getExecutiontimer() != null &&
                            RedisProcessorThread.getExecutiontimer().getSnapshot() != null &&
                            RedisProcessorThread.getExecutiontimer().getSnapshot().get75thPercentile() >= maxExecutionTime) {
                        log.error("Execution time exceeded :{} :{}", maxExecutionTime, RedisProcessorThread.getExecutiontimer().getSnapshot().get75thPercentile());
                        inDegraded = true;
                    }


                } catch (Exception e) {
                    log.error("Exception all processors up", e);
                }


                try {
                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                    log.error("Exception", e);
                }
            } catch (Exception e){
                log.error("Overload thread exception",e);
            }
        }
    }

    public void cleanParkedEvents(){
        Map<String,Object> parkedEvents = RedisProcessorThread.parkedEvents;
    }

    public static void redisExceptionSetDegraded(){
        inDegraded=true;
    }
}
