package com.eitg.quasar.occp.utils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class ConsumersCache {
    private static final Logger log = LoggerFactory.getLogger(ConsumersCache.class.getName());
    private static ConsumersCache instance=new ConsumersCache();
    Cache<String, Object> consumers = null;

    public static ConsumersCache get(){
        return instance;
    }

    private ConsumersCache(){
        consumers=Caffeine.newBuilder()
                .expireAfterWrite(2, TimeUnit.SECONDS)
                .build();
    }

    public void setConsumer(String consumer){
        log.debug("Set queue afinity:{}",consumer);
        consumers.put(consumer, Instant.now().toEpochMilli());
    }
    public Object getConsumer(String consumer){
        return consumers.getIfPresent(consumer);
    }

    public long size() {
        return consumers.estimatedSize();
    }
}
