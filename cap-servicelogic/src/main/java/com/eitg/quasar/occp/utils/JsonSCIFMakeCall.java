package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import com.eitg.quasar.occp.cap.servicelogic.CapServiceImslet;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class JsonSCIFMakeCall extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFMakeCall.class.getName());
	private CommonParameterSet commPset=null;
	private CapCallUser callUser=null;
	private Map<String,Object> rawMessage=null;


	public JsonSCIFMakeCall(Map<String, Object> rawMessage, JsonSession session){
		_log.debug("Processing make call action ...");
		Long diff = getTimeDiff(rawMessage);
		this.rawMessage = rawMessage;

		//fetch queue
		String queue = (String)rawMessage.get("ingressQueue");
		String sessionId = (String) rawMessage.get("callid");

		String queueAffinity=(String) rawMessage.get("queue");
		if( queueAffinity!=null ){
			queue=queueAffinity;
		}

		callUser = CapServiceImslet.createCall(/*session.getApplicationSession()*/);
		callUser.setSession(sessionId);
		if( session!=null ) {
			callUser.setApplicationSession(session.getApplicationSession());
		}


		String eventLockId = (String)rawMessage.get("event-id");
		callUser.setLockEventId(eventLockId);

		call = callUser.getCall();
		callUser.setQueue(queue);
		JsonSCIFRequest jsonCall = new JsonSCIFRequest(call);
		callUser.setJsonCall(jsonCall);

		_log.debug("Add call in pending calls sessions:{}",jsonCall.getCallId());
		CapCallUser.calls.put(jsonCall.getCallId(),callUser);



		Boolean fireAndForget = (Boolean) rawMessage.get(JsonSCIFResponse.FIRE_FORGET);
		if( fireAndForget!=null && callUser!=null){
			callUser.setFireAndForget(fireAndForget);
		}

		commPset = call.getParameterSet(CommonParameterSet.class);

		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);

		try {
			if( rawMessage!=null){
				_log.debug("Starting to set CEC and Header rules");

				action = (Map<String,Object>) rawMessage.get(ACTION);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_log.error("Exception",e);
		}

	}
	
	@Override
	public void execute() {
		_log.debug("executing ...");
		//create new call object

		//fetch destination address
		try {

			callUser.makeCall(rawMessage);

		} catch(Exception e){
			_log.error("Exception",e);
		}

		_log.debug("executing ... - DONE");
	}

}
