package com.eitg.quasar.occp.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.*;
import com.hp.opencall.ngin.scif.parameters.CsCallParameterSet.RedirectingReason;
import com.hp.opencall.ngin.scif.parameters.CsCallParameterSet.TcapEndMode;
import com.hp.opencall.ngin.scif.resources.*;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.servicelogic.CapServiceImslet;
import com.eitg.quasar.occp.cap.utils.TelecomUtils;
import com.eitg.quasar.occp.cap.utils.TimerInformation.TIMER_TYPE;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.ngin.scif.events.sip.InfoPollEvent;
import com.hp.opencall.ngin.scif.events.sip.RawContentPollEvent;
import com.hp.opencall.ngin.scif.events.sip.SuccessResponsePollEvent;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.Capability;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.NrtPrompt;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.NrtPromptSpec;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerListener;

import javax.management.ObjectName;
import javax.xml.bind.DatatypeConverter;

/**
 * 
 * This message is received from business logic and it contains
 * - header rules subscribed to
 * - header rules vars
 * - events subscribed to
 * - header rules ruleset
 * - timers
 * - capabilities
 * - ringing tone
 * - action
 * 		- type: 0-Abort, 1-forwardCall, 2-MRF
 * 			ABORT:
 * 				- ErrorCode
 * 				- Cause
 * 			ForwardCall
 * 				- uri
 * 				- legName
 * 			MRF:
 * 				- earlyMedia : true/false
 * 				- legName
 */
public abstract class JsonSCIFResponse {
	
	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponse.class.getName());

	public enum ActionType{
		AbortCall,
		ForwardCall,
		MRFCall,
		ContinueCall,
		LeaveCall
	}

	Map<String, Object> rawMessage;
	Call call;
	
	String rawJson=null;
	List<String> headerRules;
	Map<String,Object> events;
	Map<String,Object> headerRulesVars;
	String headerRuleSelected=null;
	List<Map<String, Object>> timers;
	List<String> capabilities;
	List<Map<String,Object>> ringingTones;
	Map<String,Object> action;
	CapCallUser capCallUser=null;
	
	
	public static final String HEADER_RULES="headerrules";
	public static final String HEADER_VARS="headerrulevar";
	public static final String HEADER_SELECT="headerrulesselect";
	public static final String EVENTS="events";
	public static final String TIMERS="timers";
	public static final String TIMEOUT = "timeout";
	public static final String TIMER_NAME = "name";
	public static final String UI_ANN = "anno_name";
	public static final String ANN_TYPE = "anno_type";
	public static final String ACTION = "action";	
	public static final String ACTION_TYPE="type";
	public static final String ERRORCODE="errorcode";
	public static final String CAUSE="cause";
	public static final String URIs="uri";
	public static final String CALLING_NUMBER="callingParty";
	public static final String EARLYMEDIA="earlymedia";
	
	
	public static final String SuccessReponseActionAccept="accept";
	public static final String SuccessReponseActionForward="forward";
	public static final String SuccessReponseActionReject="reject";

	private static final String LEG_NAME = "legname";

	public static final String LEG_ACTION = "legaction";
	public static final String LEG_RELEASE = "release";
	public static final String LEG_PLAYPROMPT = "playPrompt";
	public static final String LEG_PROMPTCOLLECT = "promptCollect";
	public static final String LEG_REJECTMEDIAOPERATION = "rejectMediaOperation";
	public static final String MEDIA_LANGUAGE = "lang";
	public static final String DISCONNECT_FROM_IP_FORBIDEN = "disconnectFromIPForbidden";
	public static final String MEDIA_REPEAT ="repeat";
	public static final String MEDIA_DURATION ="duration";
	public static final String MEDIA_DELAY="delay";

	public static final String MEDIA_NAME = "media";


	public static final String EVENTNAME = "eventname";

	public static final String CALL = "call";

	public static final String SESSION = "session";
	public static final String CALL_USER = "callUser";

	public static final String LEG = "leg";

	public static final String POLLEVENT = "pollEvent";
	public static final String FIRE_FORGET="fireAndForget";

	
	private long startTime=Calendar.getInstance().getTimeInMillis();
	
	protected Long getTimeDiff(Map<String, Object> rawMessage){
		Long startTime = (Long)rawMessage.get(JsonSCIFRequest.StartTime);
		Long stopTime = Calendar.getInstance().getTimeInMillis();
		Long diff=new Long(-1);
		if( startTime!=null && stopTime!=null){
			diff = stopTime-startTime;
		}
		return diff;
	}
	
	public JsonSCIFResponse(){
		
	}
	
	public JsonSCIFResponse(Call aCall, Map<String, Object> rawMessage, JsonSession session, CallLeg leg){
		Long diff = getTimeDiff(rawMessage);
		_log.debug("Processing response for leg :{}, timediff:{}, rawMessage:",leg,diff,rawMessage);
		call=aCall;
		SipParameterSet sipPset = call.getParameterSet(SipParameterSet.class);
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		action = (Map<String,Object>) rawMessage.get(ACTION);
		if( action!=null){
			//get action type
			Integer type = (Integer) action.get(ACTION_TYPE);
			String legAction = (String) action.get(LEG_ACTION);
			if(type!=null && type ==3 && legAction!=null) {
				//this is leg action
				processLegAction(action, leg);
			}
		}
	}
	
	
	protected Contact processAction(Map<String,Object> action){
		_log.debug("Processing action:{}",action);
		Contact ret=null;
		Integer type = (Integer)action.get(ACTION_TYPE);
		Integer errorCode = (Integer) action.get(ERRORCODE);
		String rawReleaseCallCause=(String) action.get("rawReleaseCallCause");
		String cause = (String) action.get(CAUSE);
		Cause ntwkCause = Cause.NONE;
		try {
			if (cause != null && cause.length()>0)
				ntwkCause = Cause.valueOf(cause);
		} catch (Exception e){
			_log.debug("Exception on set network cause",e);
		}

		Map<String, Object> uri = (Map<String, Object>) action.get(URIs);
		Map<String, Object> genericNumber = (Map<String, Object>) action.get(CALLING_NUMBER);
		Boolean earlyMedia = (Boolean) action.get(EARLYMEDIA);
		if( earlyMedia==null){
			earlyMedia = false;
		}
		String legName = (String) action.get(LEG_NAME);

		//action type can be:
		//0 - abort calls
		//1 - forwardCall
		//2 - connect to announcement
		//3 - continue call


		
		CsCallParameterSet csPset = (CsCallParameterSet) call.getParameterSet(CsCallParameterSet.class);
		CsSmsParameterSet csSMS = (CsSmsParameterSet) call.getParameterSet(CsSmsParameterSet.class);
		CommonParameterSet commPset = (CommonParameterSet) call.getParameterSet(CommonParameterSet.class);
		ChargingParameterSet chargPset = (ChargingParameterSet) call.getParameterSet(ChargingParameterSet.class);

		Map<String,Object> asn1Connect = (Map<String,Object>) action.get("asn1Connect");
		_log.debug("ASN1 connect:{}",asn1Connect);
		if( asn1Connect!=null){
			this.capCallUser.setAsn1Connect(asn1Connect);
			if( csPset!=null ) {
				try {
					_log.debug("Set forceSendConnect!");
					csPset.forceSendConnect();
				} catch (Exception e){
					_log.error("Exception on forceSendConnect",e);
				}
			}
		}

		Boolean forceSendConnect = (Boolean) action.get("forceSendConnect");
		if(forceSendConnect!=null && forceSendConnect==true ){
			try {
				csPset.forceSendConnect();
			} catch (Exception e){
				_log.error("Force sendConnect Exception",e);
			}
		}

		if(genericNumber!=null){
			_log.debug("Set generic number:{}",genericNumber);
			Address genericNumberAddr = TelecomUtils.mapToAddr(genericNumber);
			try {
				if( genericNumberAddr!=null ){
					List<Address> genericNBs = new ArrayList<>();
					genericNBs.add(genericNumberAddr);
					csPset.setGenericNumbers(genericNBs);
				}
//				commPset.setCallingParty(genericNumberAddr);
			} catch (ScifException e) {
				_log.error("Exception:",e);
			}
		}

		//original called party id
		Map<String, Object> originalCalledPartyID = (Map<String, Object>) action.get("originalCalledPartyID");
		_log.debug("OrigCdPA:{}",originalCalledPartyID);
		if( originalCalledPartyID!=null ){
			Address origCdPA = TelecomUtils.mapToAddr(originalCalledPartyID);
			try {
				csPset.setOriginalCalledPartyID(origCdPA);
			} catch (Exception e){
				_log.error("Error set originalCalledPartyID");
			}
		}

		//set redirecting party id
		Map<String, Object> redirectingPartyID = (Map<String, Object>) action.get("redirectingPartyID");
		_log.debug("RedirPA:{}",redirectingPartyID);
		if( redirectingPartyID!=null ){
			Address redirPa = TelecomUtils.mapToAddr(redirectingPartyID);
			try {
				commPset.setRedirectingParty(redirPa);
			} catch (Exception e){
				_log.error("Error set originalCalledPartyID");
			}
		}

		String redirectInfo = (String) action.get("redirectionInformation");
		_log.debug("RedirInfo:{}",redirectInfo);
		if( redirectInfo!=null ){
			try {
				byte[] tmp = DatatypeConverter.parseHexBinary(redirectInfo);
				csPset.setRawRedirectionInformation(tmp);
			} catch (Exception e){
				_log.error("Error set redirect information");
			}
		}


		//set calling party category
		Integer callingPartyCategory = (Integer)action.get("callingPartyCategory");
		if( callingPartyCategory!=null){
			csPset.setCallingPartysCategory(callingPartyCategory);
		}

		//set duration
		Map<String, Object> duration = (Map<String, Object>) action.get("duration");
		if(duration!=null){
			Integer amount = (Integer) duration.get("amount");
			Integer isFinal = (Integer) duration.get("final");

			if( amount!=null) {
                ChargingUnitsExt[] chargingUnits = new ChargingUnitsExt[1];
                chargingUnits[0] = new ChargingUnitsExt();
                amount = amount * 1000;
                BigInteger bigAmount = BigInteger.valueOf(amount);
                chargingUnits[0].setBigAmount(bigAmount);
                chargingUnits[0].setUnit(ChargingUnits.Unit.TIME_MILLISECONDS);

                if (isFinal != null) {
                    chargingUnits[0].setLastUnits((isFinal == 1));
					ChargingParameterSet.GrantedUnitsWarningTone tone = new ChargingParameterSet.GrantedUnitsWarningTone();
					chargingUnits[0].setWarningTimeDelay(tone);
                }

                _log.debug("Grant units:{}", chargingUnits);
                try {
                    chargPset.setGrantedUnits(chargingUnits);

                } catch (ScifException e) {
                    _log.error("Error on grating time units", e);
                }
            }
		}

		try {
			//set FCI in case is provided
			String fci = (String) action.get("fci");
			if (fci != null) {
				FurnishChargingInfo fciOp = new FurnishChargingInfo();
				byte[] fciBytes = DatatypeConverter.parseHexBinary(fci);
				fciOp.setData(fciBytes);
				//set default to overwrite data
				fciOp.setAppendOrNot(ChargingParameterSet.FciMode.OVERWRITE);
				//hardcoded value

				Integer chargingId = (Integer) action.get("fci.chargingId");
				if( chargingId!=null ){
					fciOp.setChargingId(chargingId);
				}

				_log.debug("Sending fci:{}", fciOp);
				try {
					chargPset.setFurnishChargingInformation(fciOp);
				} catch (ScifException e) {
					_log.error("Exception in fci", e);
				}
			}
		} catch (Exception e){
			_log.error("Exception on handling fci,",e);
		}

        //set calll information request
        List<String> infoRequest = (List<String>) action.get("requestedInformation");
		if( infoRequest!=null){
			_log.debug("Set CIR:{}",infoRequest);
		    List<CsCallParameterSet.RequestedInformationList> cirs = new ArrayList<>();
		    for( String infoReq : infoRequest){
                CsCallParameterSet.RequestedInformationList reqInfo=null;
		        if( infoReq.equalsIgnoreCase("CALL_STOP_TIME")) {
                    reqInfo = CsCallParameterSet.RequestedInformationList.CALL_STOP_TIME;
                } else if (infoReq.equalsIgnoreCase("CALL_CONNECTED_ELAPSED_TIME")) {
                    reqInfo = CsCallParameterSet.RequestedInformationList.CALL_CONNECTED_ELAPSED_TIME;
                }
                if( reqInfo!=null ) {
                    cirs.add(reqInfo);
                }
            }
            if( cirs.size()>0) {
				_log.debug("Sending CIR:{}", cirs);
				csPset.setCallInfoReqList(cirs);
			}
        }

		if( csSMS!=null){
			try {
				Map<String, Object> smscAddress = (Map<String, Object>) action.get("smscAddress");
				_log.debug("set SMSC address:{}",smscAddress);
				if (smscAddress != null) {
					Address address = TelecomUtils.mapToAddr(smscAddress);
					csSMS.setSmscAddress(address);
				}
			} catch (Exception e){
				_log.error("Exception",e);
			}
		}



		ActionType actType = ActionType.values()[type];

		switch(actType){
			case AbortCall:
				_log.debug("Sending abort call errorCode:{}, cause:{} releaseCause:{}",errorCode,ntwkCause,rawReleaseCallCause);
				//this is abort call,
                //send TC_END
				Cause errorCause = Cause.NONE;
				if( errorCode!=null){

					errorCause =Cause.REJECTED;
					_log.debug("Set error code:{} cause:{}",errorCode,errorCause);
					commPset.setErrorCode(errorCode);
				}

				if( rawReleaseCallCause!=null ){
					if( csPset!=null ) {
						try {
							csPset.setRawReleaseCallCause(DatatypeConverter.parseHexBinary((String) rawReleaseCallCause));
						} catch (Exception e) {
							_log.error("Exception setRawReleaseCallCause",e);
						}
					}
					if( csSMS!=null ){
						try {
							_log.debug("Sending raw release case:{}",rawReleaseCallCause);
							csSMS.setRawReleaseCallCause(DatatypeConverter.parseHexBinary((String) rawReleaseCallCause));

						} catch (Exception e) {
							_log.error("Exception SMS setRawReleaseCallCause",e);
						}
					}
				}
				call.abortCall(errorCause);
				break;
			case ForwardCall:
				Address address = TelecomUtils.mapToAddr(uri);
				TerminalContact termContact = new TerminalContact(address);
				ret = termContact;
				try {
					call.forwardCall(termContact);
				} catch (ScifException e) {
					_log.error("Exception",e);
				}
				_log.debug("Sending forward call contact:{}",ret);
			break;
			case MRFCall:
				try {
					String mediaServer=null;
					Boolean csDrivenMrf = (Boolean) action.get("csDrivenMrf");
					Object waitCorrelationId = action.get("getCorrelationId");
					if( csDrivenMrf!=null ){
						csPset.setCSDrivenMRF(csDrivenMrf);
					}
					mediaServer = (String)action.get("mediaServer");
					IvrContact ivrContact = ScifFactory.instance().getResourceContact(
							call,
							IvrContact.class,
							mediaServer);
					ivrContact.setLegName("ivr");
					ivrContact.setEarlyMedia(earlyMedia);


					ret = ivrContact;
					call.forwardCall(ret);

					_log.debug("Call correlation id:{}",csPset.getCorrelationId());
					if( waitCorrelationId!=null ){
						//wait for correlation id
						//build callPoll event
						if( this.capCallUser!=null ){
							_log.debug("Sending custom call poll event");
							Map<String,Object> callPollCustom = new HashMap<>();
							callPollCustom.put("correlationId",csPset.getCorrelationId());
							this.capCallUser.sendCallPollEvent(callPollCustom);
						}
					}


				} catch (ScifException e) {
					_log.error("Exception",e);
				}
			
			_log.debug("Sending forward call to IVR contact:{}",ret);
			break;
			case ContinueCall:
				try {
					_log.debug("Continue call");
					call.continueCall();
				} catch (ScifException e) {
					_log.error("Continue call exception",e);
					e.printStackTrace();
				}
				break;
			case LeaveCall:
				try {
					_log.debug("Leave call uri:{}",uri);
					TerminalContact leaveCallContact=null;

					if( uri!=null) {
						Address leaveCallAddr = null;
						if (uri != null) {
							leaveCallAddr = TelecomUtils.mapToAddr(uri);
							leaveCallContact = new TerminalContact(leaveCallAddr);
						}
						ret = leaveCallContact;

						String scheme = (String) uri.get("scheme");
						if (scheme != null && !scheme.equalsIgnoreCase("tel")) {
							_log.debug("Set address fwd parameters ...");
							Address.NetworkContext nwkCtx = new Address.NetworkContext(Address.Q713Standard.STANDARD_NAME,
									Address.Q713Standard.Nai.INTERNATIONAL_NUMBER,
									Address.Q713Standard.Npi.E163_E164);

							leaveCallAddr.setParameter(Address.Parameter.NETWORK_CONTEXT, nwkCtx.toString());
							leaveCallAddr.setParameter(Address.Parameter.GT_INDICATOR, Address.GtIndicator.TYPE4);
							leaveCallAddr.setParameter(Address.Parameter.GT_TRANSLATION_TYPE, Address.GtTranslationtype.UNUSED);
						}
					}
					_log.debug("Sending leaveCall to:{}",leaveCallContact);

					call.leaveCall(Cause.NONE, leaveCallContact);
				} catch (ScifException e){
					_log.error("Exception",e);
				}
				break;
		}

		
		return ret;
	}
	
	protected void processLegAction(Map<String, Object> action, CallLeg leg){
		_log.debug("legAction:{} leg:{}",action,leg);
		//is assumed that leg is of type RawMedia
		if( leg instanceof IvrCallLeg){
			IvrCallLeg ivrLegMedia = (IvrCallLeg) leg;
			String legAction = (String) action.get(LEG_ACTION);
			Map<String, Object> legActionMap = (Map<String, Object> )action.get(legAction);
			_log.debug("actionLeg:{} map:{}",legAction,legActionMap);
			if( legActionMap!=null){
				if( legAction.equalsIgnoreCase(LEG_RELEASE)){
					_log.debug("Action - RELEASE leg ...");
					//fetch cause
					String cause = (String)legActionMap.get(CAUSE);
					Cause relCause = Cause.NONE;
					if( cause!=null){
						//relCause = Cause.valueOf(cause);
					}
					try {
						_log.debug("releasing leg:{}",ivrLegMedia.toString());
						leg.release(relCause);
					} catch (ScifException e) {
						_log.error("PlayAnno Exception ",e);
					}
				} else if (legAction.equalsIgnoreCase(LEG_PLAYPROMPT)){
					_log.debug("Performing media operation ...");
					String mediaName = (String)legActionMap.get(MEDIA_NAME);
					String language = (String) legActionMap.get(MEDIA_LANGUAGE);
					Boolean disconnectFromIPForbidden=(Boolean) legActionMap.get(DISCONNECT_FROM_IP_FORBIDEN);

					
					Properties props = new Properties();
					
					String repeat = (String) legActionMap.get(MEDIA_REPEAT);
					if(repeat!=null){
						props.setProperty(MEDIA_REPEAT, repeat);
					}
					String duration = (String) legActionMap.get(MEDIA_DURATION);
					if(duration!=null){
						props.setProperty(MEDIA_DURATION, duration);
					}
					String delay = (String) legActionMap.get(MEDIA_DELAY);
					if(delay!=null){
						props.setProperty(MEDIA_DELAY, delay);
					}
					
					Map<String,Object> variables = (Map<String,Object>)legActionMap.get("variables");
					Collection<VoiceVariable> vars = new ArrayList<>();
					if(variables!=null && variables.size()>0){
						_log.debug("Adding variable parts:{}",variables);
						for( Map.Entry<String,Object> entry : variables.entrySet()){
							Map<String,Object> var = (Map<String,Object>)entry.getValue();
							String value = (String)var.get("value");
							String[] annoVars=value.split(":");
							_log.debug("Adding variable value:{} parts:{}",value,annoVars);
							switch(annoVars[0].toLowerCase(Locale.ROOT)) {
								case "string":
									vars.add(new VoiceVariable.String(annoVars[1]));
									break;
								case "count":
									try {
										vars.add(new VoiceVariable.Count(Integer.parseInt(annoVars[1])));
									} catch (Exception e){
										_log.error("Exception parsing",e);
									}
									break;
								case "currency":
									try {
										//parse currency
										String curStr = TelecomUtils.bcdToString(annoVars[1]);
										Float tmp = Float.parseFloat(curStr);
										tmp = tmp/100;
										vars.add(new VoiceVariable.Currency(tmp));
									} catch (Exception e){
										_log.error("Exception convers to float",e);
									}
									break;
								case "date":
									try {
										String currDate = TelecomUtils.bcdToString(annoVars[1]);
										Calendar cal = Calendar.getInstance();
										SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
										cal.setTime(sdf.parse(currDate));// all done

										vars.add(new VoiceVariable.Date(cal));
									} catch (Exception e){
										_log.error("Exception convers to date",e);
									}
									break;
								case "digit":
									String currDigit=TelecomUtils.bcdToString(annoVars[1]);
									vars.add(new VoiceVariable.Digit(currDigit));
									break;
								case "duration":
									vars.add(new VoiceVariable.Duration(annoVars[1]));
									break;
								case "time":
									try {
										String currDate = TelecomUtils.bcdToString(annoVars[1]);
										Calendar cal = Calendar.getInstance();
										SimpleDateFormat sdf = new SimpleDateFormat("hhmmss", Locale.ENGLISH);
										cal.setTime(sdf.parse(currDate));// all done

										vars.add(new VoiceVariable.Time(cal));
									} catch (Exception e){
										_log.error("Exception convers to date",e);
									}
									
									break;
							}
						}

					}

					_log.debug("Play annoucemement with anno:[{}] variables:{}",
							"CamelService/"+mediaName,
							vars);

					MediaOperation mediaOperation = new MediaOperation("CamelService/"+mediaName,vars);
					if( language!=null) {
						mediaOperation.setLanguage(language);
					}
					
					if (!props.isEmpty()){
						_log.debug("Set parameters:{}",props);
						mediaOperation.setParameters(props);
					}
					
					try {
						if (disconnectFromIPForbidden==null || disconnectFromIPForbidden==false ) {
							Properties properties= new Properties();
							properties.put("IS_LAST_MEDIA_OPERATION", true);
							mediaOperation.setParameters(properties);
						}
						ivrLegMedia.play(mediaOperation);
					} catch(Exception e){
						_log.error("Error playing announcement",e);
					}

				}	else if (legAction.equalsIgnoreCase(LEG_PROMPTCOLLECT)){
					_log.debug("Performing media operation ...");
					String mediaName = (String)legActionMap.get(MEDIA_NAME);
					String language = (String) legActionMap.get(MEDIA_LANGUAGE);
					Boolean disconnectFromIPForbidden=(Boolean) legActionMap.get(DISCONNECT_FROM_IP_FORBIDEN);

					Map<String,Object> variables = (Map<String,Object>)legActionMap.get("variables");
					Collection<VoiceVariable> vars = new ArrayList<>();
					if(variables!=null && variables.size()>0){
						for( Map.Entry<String,Object> entry : variables.entrySet()){
							String value = (String)entry.getValue();
							String[] annoVars=value.split(":");
							_log.debug("Adding variable part:{}",annoVars);
							switch(annoVars[0].toLowerCase(Locale.ROOT)){
								case "string":
									vars.add(new VoiceVariable.String(annoVars[1]));
									break;
								case "count":
									vars.add(new VoiceVariable.Count(Integer.parseInt(annoVars[1])));
									break;
								case "currency":
									try {
										Float tmp = Float.parseFloat(annoVars[1]);
										vars.add(new VoiceVariable.Currency(tmp));
									} catch (Exception e){
										_log.error("Exception parsing currency to float",e);
									}
									break;
								case "date":
									vars.add(new VoiceVariable.Date(annoVars[1]));
									break;
								case "digit":
									vars.add(new VoiceVariable.Digit(annoVars[1]));
									break;
								case "duration":
									vars.add(new VoiceVariable.Duration(annoVars[1]));
									break;
								case "time":
									vars.add(new VoiceVariable.Time(annoVars[1]));
									break;
							}
						}
						_log.debug("variables:{}",vars);
					}


					MediaOperation mediaOperation = new MediaOperation("CamelService/"+mediaName,vars);

					if( language!=null) {
						mediaOperation.setLanguage(language);

					}
					try {
						if (disconnectFromIPForbidden==null || disconnectFromIPForbidden==false) {
							Properties properties= new Properties();
							properties.put("IS_LAST_MEDIA_OPERATION", true);
							mediaOperation.setParameters(properties);
						}

						ivrLegMedia.playCollect(mediaOperation);
					} catch(Exception e){
						_log.error("Error playing prompt announcement",e);
					}

				}
			}
		}
	}
	
	
	public abstract void execute();

	static JsonSCIFResponse getResponse(Map<String, Object> rawResponse, Map<String, Object> eventData){
		_log.debug("getResponse eventData:{} message:{}",eventData, rawResponse);
		JsonSCIFResponse resp=null;
		
		String eventName = (String) rawResponse.get(EVENTNAME);
		Integer eventId = (Integer) rawResponse.get(JsonSCIFRequest.EventId);
		String eventIdUniq=(String) rawResponse.get(JsonSCIFRequest.EVENTIdentifier);

		Call call = null;
		JsonSession session = null;
		CapCallUser callUser = null;

		if( eventData!=null ) {
			callUser = (CapCallUser) eventData.get(JsonSCIFResponse.CALL_USER);
			if (callUser != null && eventIdUniq != null) {
				//in case of make call, skip to set it
				if( !eventName.equalsIgnoreCase("makeCall")) {
					callUser.setLockEventId(eventIdUniq);
				}
			}
			if (eventData != null) {
				call = (Call) eventData.get(CALL);
				session = (JsonSession) eventData.get(SESSION);
			}
		}

		if( callUser!=null ){
			String queueAffinity=(String )rawResponse.get("queue");
			if( queueAffinity!=null ) {
				callUser.setQueue(queueAffinity);
			}
		}

		if( eventName.equalsIgnoreCase(JsonSCIFRequest.CALLSTART)){
			resp = new JsonSCIFResponseCallStart(call, rawResponse, session,callUser);
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLPOLL)) {
			ScifEvent pollEvent = (ScifEvent) eventData.get(POLLEVENT);

			resp = new JsonSCIFResponseCallPoll(call, rawResponse, session, pollEvent);

			
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLANSWERED) || 
				eventName.equalsIgnoreCase(JsonSCIFRequest.CALLEARLYANSWERED) ||
				eventName.equalsIgnoreCase(JsonSCIFRequest.PLAYCOMPLETE) ||
				eventName.equalsIgnoreCase(JsonSCIFRequest.COLLECTCOMPLETE)){

			//leg actions are related to events: callAnswered, callEarlyAnswered, PlayComplete, CollectComplete

			_log.debug("CallUser:{}",callUser);
			CallLeg leg = (CallLeg) eventData.get(LEG);
			if( leg ==null ){
				if( callUser!=null ){
					leg = callUser.getSrfLeg();
				}
			}
			resp = new JsonSCIFResponseLegAction(call, rawResponse, session, leg);
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.MAKECALL)){
			resp = new JsonSCIFMakeCall(rawResponse,session);
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.VSSF)){
			resp = new JsonSCIFResponseVSSF(call, callUser , rawResponse,session);
		}
		
		return resp;
	}

	
	public long getTimeDiff(){
		return Calendar.getInstance().getTimeInMillis()-startTime;
	}
	
	private void csMapToCsParameterSet(Map<String, Object> csJson, CsCallParameterSet csPset){
		if( csJson==null){
			return;
		}
		
		Object value = csJson.get("alertingPattern");
		if( value!=null){
			csPset.setAlertingPattern((Integer) value);
		}
		
		value = csJson.get("applyOCSI");
		if( value!=null){
			csPset.applyOCSI();
		}
		
		value = csJson.get("detectionPointSet");
		if( value!=null){
			try {
				csPset.setDetectionPointSet((String) value);
			} catch (ScifException e) {
				_log.error("detectionPointSet Exception",e);
			}
		}
		
		value = csJson.get("redirectingReason");
		if( value!=null){
			RedirectingReason rr = RedirectingReason.valueOf((String)value);
			csPset.setRedirectingReason(rr);
		}
		
		value = csJson.get("tcapEndMode");
		if( value!=null){
			TcapEndMode te = TcapEndMode.valueOf((String)value);
			try {
				csPset.tcapEndMode(te);
			} catch (ScifException e) {
				_log.error("tcapEndMode Exception",e);
			}
		}
		
	}
}
