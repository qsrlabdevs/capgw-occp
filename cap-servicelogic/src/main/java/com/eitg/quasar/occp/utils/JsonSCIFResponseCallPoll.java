package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.utils.TelecomUtils;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.cs.events.InCallChargingEvent;
import com.hp.opencall.ngin.scif.events.ChargingPollEvent;
import com.hp.opencall.ngin.scif.parameters.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Map;

public class JsonSCIFResponseCallPoll extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseCallPoll.class.getName());

	private ScifEvent pollEvent = null;

	public JsonSCIFResponseCallPoll(Call aCall, Map<String, Object> rawMessage, JsonSession session, ScifEvent pollEvent){
//		super(aCall, rawMessage, session);
		
		Long diff = getTimeDiff(rawMessage);
		call =aCall;
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);
		
		try {
			if( rawMessage!=null){
				//8. action: forward call
				action = (Map<String,Object>) rawMessage.get(ACTION);
			}
		} catch (Exception e) {
			_log.error("Exception",e);
		}
		this.pollEvent = pollEvent;
	}


	private void processPollAction(Map<String, Object> action){

		Integer type = (Integer)action.get(ACTION_TYPE);
		if(type==null){
			type=0;
		}

		//type
		//-1 - continue
		//-0 - drop call

		CsCallParameterSet csPset = (CsCallParameterSet) call.getParameterSet(CsCallParameterSet.class);
		CommonParameterSet commPset = (CommonParameterSet) call.getParameterSet(CommonParameterSet.class);
		ChargingParameterSet chargPset = (ChargingParameterSet) call.getParameterSet(ChargingParameterSet.class);



		if( type==1) {
			//set duration
			Map<String, Object> duration = (Map<String, Object>) action.get("duration");
			if (duration != null) {
				//Integer amount = (Integer) duration.get("amount");
				Object value = duration.get("amount");

				Integer amount=0;
				if( value instanceof Double){
					amount = ((Double) value).intValue();
				} else if( value instanceof Long){
					amount =((Long) value).intValue();
				} else if( value instanceof Integer){
					amount = (Integer) value;
				} else if ( value instanceof  String){
					try {
						amount = Integer.parseInt((String) value);
					} catch (Exception e){
						_log.error("Exception parsing amount",e);
					}
				}

				Integer isFinal = (Integer) duration.get("final");

				_log.debug("amount:{} final:{}",amount,isFinal);

				if (amount != null) {
					ChargingUnitsExt[] chargingUnits = new ChargingUnitsExt[1];
					chargingUnits[0] = new ChargingUnitsExt();
					amount = amount * 1000;
					BigInteger bigAmount = BigInteger.valueOf(amount);
					chargingUnits[0].setBigAmount(bigAmount);
					chargingUnits[0].setUnit(ChargingUnits.Unit.TIME_MILLISECONDS);

					if (isFinal != null) {
						chargingUnits[0].setLastUnits((isFinal == 1));
						ChargingParameterSet.GrantedUnitsWarningTone tone = new ChargingParameterSet.GrantedUnitsWarningTone();
						chargingUnits[0].setWarningTimeDelay(tone);
					}

					_log.debug("Grant units:{}", chargingUnits);
					try {
						chargPset.setGrantedUnits(chargingUnits);
					} catch (ScifException e) {
						_log.error("Error on grating time units", e);
					}
				}
			}

			if(pollEvent instanceof ChargingPollEvent){
				_log.debug("Granting new duration ...");
				ChargingPollEvent chargingEvent = (ChargingPollEvent) pollEvent;
				chargingEvent.accept();


			}
		} else if (type==0) {
			if(pollEvent instanceof ChargingPollEvent){
				_log.debug("Rejecting call poll ..");
				ChargingPollEvent chargingEvent = (ChargingPollEvent) pollEvent;
				chargingEvent.reject(Cause.REJECTED);
			}
		} else {
			processAction(action);
		}

	}

	@Override
	public void execute() {
		_log.debug("Executing ... ");
		try{
			processPollAction(action);

		} catch (Exception e){
			e.printStackTrace();
		}
		_log.debug("Executing ... - DONE");
	}
}
