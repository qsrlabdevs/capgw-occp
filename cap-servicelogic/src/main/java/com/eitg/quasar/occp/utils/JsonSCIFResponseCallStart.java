package com.eitg.quasar.occp.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet.Capability;

public class JsonSCIFResponseCallStart extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseCallStart.class.getName());
	
	public JsonSCIFResponseCallStart(Call aCall, Map<String, Object> rawMessage, JsonSession session, CapCallUser callUser){
//		super(aCall, rawMessage, session);
		
		Long diff = getTimeDiff(rawMessage);
		call =aCall;
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		capCallUser = callUser;
		
		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);
		
		try {
			if( rawMessage!=null){
				//8. action: forward call
				action = (Map<String,Object>) rawMessage.get(ACTION);
			}
		} catch (Exception e) {
			_log.error("Exception",e);
		}
	}

	@Override
	public void execute() {
		_log.debug("Executing , process action:{}",action);
		try{
			Contact contact = processAction(action);
//			if( contact!=null){
//				_log.debug("Forward call to:{}",contact);
//				call.forwardCall(contact);
//			}
		} catch (Exception e){
			_log.error("Exception",e);
		}
		_log.debug("Executing ... - DONE");
	}
}
