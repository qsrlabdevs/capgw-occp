package com.eitg.quasar.occp.utils;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import com.eitg.quasar.occp.cap.utils.TelecomUtils;
import com.hp.opencall.camel.camel2.cap_datatypes.CollectedDigits;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.cs.events.*;
import com.hp.opencall.ngin.scif.cs.events.sms.InSmsMonitorEvent;
import com.hp.opencall.ngin.scif.cs.events.sms.InSmsSubmittedEvent;
import com.hp.opencall.ngin.scif.events.ChargingPollEvent;
import com.hp.opencall.ngin.scif.parameters.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.lang.reflect.Parameter;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public class JsonSCIFResponseVSSF extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseVSSF.class.getName());

	private ScifEvent pollEvent = null;
	private CapCallUser callUser;

	public JsonSCIFResponseVSSF(Call call, CapCallUser callUser, Map<String, Object> rawMessage , JsonSession session ){
//		super(aCall, rawMessage, session);
		
		Long diff = getTimeDiff(rawMessage);
		this.call=call;
		this.callUser=callUser;
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);
		
		try {
			if( rawMessage!=null){
				//8. action: forward call
				action = rawMessage;
			}
		} catch (Exception e) {
			_log.error("Exception",e);
		}
		this.pollEvent = pollEvent;
	}


	private void processVssfAction(Map<String, Object> action){
		List<Map<String,Object>> components = (List<Map<String,Object>>)action.get("components");
		//type
		//-1 - continue
		//-0 - drop call

		CsCallParameterSet csPset = (CsCallParameterSet) call.getParameterSet(CsCallParameterSet.class);
		CommonParameterSet commPset = (CommonParameterSet) call.getParameterSet(CommonParameterSet.class);
		ChargingParameterSet chargPset = (ChargingParameterSet) call.getParameterSet(ChargingParameterSet.class);


		boolean terminateCall=false;

		for(Map<String,Object> component : components){
			String compName = (String)component.get("name");
			if( compName.equalsIgnoreCase("erb")){
				processERB(component);
			} else if(compName.equalsIgnoreCase("acr")){
				processACR(component);
				Boolean terminate = (Boolean) component.get("final");
				if( terminate!=null && terminate==true ){
					terminateCall=true;
				}
			} else if(compName.equalsIgnoreCase("terminate")){
				_log.debug("Received component - Terminating call");

				String rawReleaseCause = (String)component.get("releaseCause");
				if( rawReleaseCause!=null){
					try {
						byte[] bufReleaseCause = DatatypeConverter.parseHexBinary(rawReleaseCause);
						csPset.setRawReleaseCallCause(bufReleaseCause);
					} catch (Exception e){
						_log.error("Exception set raw release cause",e);
					}
				}
				call.abortCall(Cause.NONE);
			} else if(compName.equalsIgnoreCase("media")){
				processMedia(component);
			} else if(compName.equalsIgnoreCase("mediaDisconnect")){
				processMediaDisconnect(component);
			} else if(compName.equalsIgnoreCase("mediaPA")){
				processMediaPA(component);
			} else if(compName.equalsIgnoreCase("mediaPAC")){
				processMediaPAC(component);
			} else {
				_log.error("Unknown component to process:{}",component);
			}
		}

		if ( terminateCall ){
			_log.debug("Terminating call");
			call.abortCall(Cause.NONE);
		}


	}

	private void processERB(Map<String,Object> component){
		_log.debug("Processing ERB:{}",component);
		String cause = (String)component.get("cause");
		Integer legId = (Integer) component.get("legId");
		Boolean last = (Boolean) component.get("lastEvent");
		Map<String,Object> asn1 = (Map<String,Object>)component.get("asn1");
		if( asn1!=null){
			this.callUser.setAsn1Erb(asn1);
		}

		int eventType=-1;
		int eventTypeSMS=-1;
		if( cause.equalsIgnoreCase("answer")){
			eventType= InCallAnswerEvent.IN_CALL_ANSWER_POLL_EVENT_TYPE;
		} else if (cause.equalsIgnoreCase("disconnected")){
			eventType= InCallDisconnectEvent.IN_CALL_DICONNECT_POLL_EVENT_TYPE;
		} else if (cause.equalsIgnoreCase("busy")){
			eventType= InCallBusyEvent.IN_CALL_BUSY_POLL_EVENT_TYPE;
		} else if (cause.equalsIgnoreCase("no_answer")){
			eventType= InCallNoAnswerEvent.IN_CALL_NOANSWER_POLL_EVENT_TYPE;
		} else if (cause.equalsIgnoreCase("not_reachable")){
			eventType= InCallRouteSelectFailureEvent.IN_CALL_ROUTESELECTFAILURE_POLL_EVENT_TYPE;
		} else if (cause.equalsIgnoreCase("abandon")){
			eventType= InCallAbandonEvent.IN_CALL_ABANDON_POLL_EVENT_TYPE;
		}  else if (cause.equalsIgnoreCase("submit")){
			eventTypeSMS= InSmsSubmittedEvent.IN_SMS_SUBMITTED_POLL_EVENT_TYPE;
		}   else if (cause.equalsIgnoreCase("failure")){
			eventTypeSMS= 33;
		}



		_log.debug("Processing event type:{} event typeSMS:{}",eventType,eventTypeSMS);
		if( eventType>=0) {
			//search for event
			List<InCallMonitorEvent> monitorEvents = this.callUser.getVssfMonitorEvents();
			_log.debug("Monitor events size:{}",monitorEvents.size());
			for (InCallMonitorEvent event : monitorEvents) {
				if (event.getType() == eventType){
					if( eventType==InCallDisconnectEvent.IN_CALL_DICONNECT_POLL_EVENT_TYPE ){
						if(legId!=null && event.getLegID()!=legId){
							continue;
						}
						_log.debug("For disconnect set last event true:");
						((InCallDisconnectEvent) event).setLastEvent(true);
					}
					EventReportInformation eventReport = new EventReportInformation();
					boolean setEventInfo=false;
					if( legId!=null ){
						eventReport.setLegID(legId);
						setEventInfo=true;
					}

					if( setEventInfo) {
						this.call.getParameterSet(CsCallParameterSet.class).setEventReportInformation(eventReport);
					}

					_log.debug("Accept event voice:{} legID:{}",eventType,legId);
					event.accept();
					return;
				}
			}
		}
		if( eventTypeSMS>=0 ){
			List<InSmsMonitorEvent> monitorEvents = this.callUser.getVssfSMSMonitorEvents();
			for (InSmsMonitorEvent event : monitorEvents) {
				if (event.getType() == eventTypeSMS){
					_log.debug("Accept event sms:{}",eventType);
					event.accept();
				}
			}
		}

	}

	private void processACR(Map<String,Object> component){
		_log.debug("Processing ACR ...");
		Object valDuration =  component.get("duration");
		Double duration = null;
		if( valDuration instanceof Double){
			duration =(Double) valDuration;
		} else if (valDuration instanceof Long){
			duration = ((Long)valDuration).doubleValue();
		} else if (valDuration instanceof Integer){
			duration = ((Integer) valDuration).doubleValue();
		}
		Boolean lastEvent = (Boolean) component.get("lastEvent");
		Boolean finalUnits = (Boolean) component.get("final");

		InCallChargingEvent chargingEvent = this.callUser.getVssfChargingEvent();
		if( chargingEvent!=null && duration!=null){
			try {
				ChargingParameterSet chargingParameterSet = (ChargingParameterSet) this.call.getParameterSet(ChargingParameterSet.class);
				if (chargingParameterSet != null) {
					ChargingUnitsExt[] vssfUsedUnits = new ChargingUnitsExt[1];
					vssfUsedUnits[0] = new ChargingUnitsExt();
					vssfUsedUnits[0].setUnit(ChargingUnits.Unit.TIME_MILLISECONDS);
					long durationLong = (long)(duration * 1000);
					BigInteger value = new BigInteger(String.valueOf(durationLong));
					vssfUsedUnits[0].setBigAmount(value);
					vssfUsedUnits[0].setDirection(ChargingUnits.Direction.NOT_APPLICABLE);
					vssfUsedUnits[0].setTariffChangeUsage(ChargingUnits.TariffChangeUsage.NOT_APPLICABLE);
					vssfUsedUnits[0].setTariffTimeChange(0);
					if ( finalUnits!=null ) {
						vssfUsedUnits[0].setLastUnits(finalUnits);
					}
					chargingParameterSet.setUsedUnits(vssfUsedUnits);

					_log.debug("Set used units:{}",vssfUsedUnits);
					if( lastEvent!=null && lastEvent==true) {
						chargingEvent.setLastEvent(true);
					} else {
						chargingEvent.setLastEvent(false);
					}

					if( finalUnits==null || finalUnits==false ) {
						chargingEvent.accept();
					}

				}


			} catch (Exception e){
				_log.error("Exception",e);
			}
		}
	}

	private void processMedia(Map<String,Object> component){
		_log.debug("Processing Media:{}",component);
		String cause = (String)component.get("action");
		String correlationId = (String) component.get("correlationId");
		String ipspCapabilities = (String) component.get("ipspCapabilities");

		InCallDirectMediaEvent mediaEvent = this.callUser.getVssfMediaEvent();
		if( cause.equalsIgnoreCase("accept")){
			if( mediaEvent!=null ){
				_log.debug("Sending ARI correlationId:{} ipspCapabilities:{}...",correlationId,ipspCapabilities);
				CsCallParameterSet csPset = (CsCallParameterSet) this.call.getParameterSet(CsCallParameterSet.class);
				try {
					Map<String,Object> sccpAddress = (Map<String,Object>) component.get("sccpCalledParty");
					if( sccpAddress!=null) {

						Address sccpdestAddress = TelecomUtils.mapToAddr(sccpAddress);
						_log.debug("ARI scpAddress:{}", sccpdestAddress);
						csPset.setSccpCalledPartyAddress(sccpdestAddress);
					}
				} catch (Exception e){
					_log.error("Exception set ARI dest GT",e);
				}
				try {
					csPset.setARICorrelationID(DatatypeConverter.parseHexBinary(correlationId));
					csPset.setARIIPSSPCapabilities(DatatypeConverter.parseHexBinary(ipspCapabilities));
				} catch (Exception e){
					_log.error("Exception",e);
				}
				mediaEvent.accept();
			}
		}  else {
			if( mediaEvent!=null){
				mediaEvent.reject(Cause.NONE);
			}
		}
	}

	private void processMediaPA(Map<String,Object> component){
		_log.debug("Processing Media PA:{}",component);
		String cause = (String)component.get("action");
		InCallMediaPAEvent paEvent = this.callUser.getVssfMediaPAEvent();
		if( paEvent!=null ){
			if( cause.equalsIgnoreCase("accept")){
				paEvent.accept();
			} else {
				paEvent.reject(Cause.NONE);
			}
		}
	}

	private void processMediaPAC(Map<String,Object> component){
		_log.debug("Processing Media PAC:{}",component);
		String cause = (String)component.get("action");
		String collectedDigits=(String) component.get("digits");
		InCallMediaPACEvent pacEvent = this.callUser.getVssfMediaPACEvent();

		if( pacEvent!=null ){
			if( cause.equalsIgnoreCase("accept")){
				pacEvent.setDigits(collectedDigits);
				pacEvent.accept();
			} else {
				pacEvent.reject(Cause.NONE);
			}
		}
	}

	private void processMediaDisconnect(Map<String,Object> component){
		_log.debug("Processing Media Disconnect:{}",component);
		String cause = (String)component.get("action");
		InCallDiscMediaEvent discMediaEvent = this.callUser.getVssfdiscMediaEvent();
		if( discMediaEvent!=null ){
			boolean acceptMediaDisconnect =this.callUser.acceptMediaDisconnect();
			_log.debug("Accept media disconnect:{}",acceptMediaDisconnect);
			if( acceptMediaDisconnect ) {
				if (cause.equalsIgnoreCase("accept")) {
					discMediaEvent.accept();
				} else {
					discMediaEvent.reject(Cause.NONE);
				}
			} else {
				try {
					_log.error("Force accepting media disconnect!");
					discMediaEvent.accept();
				} catch (Exception e){
					_log.error("Exception sending TC-END");
					e.printStackTrace();
				}
			}
		}
	}


	@Override
	public void execute() {
		_log.debug("Executing ... ");
		try{
			processVssfAction(action);
		} catch (Exception e){
			_log.error("Exception",e);
		}
		_log.debug("Executing ... - DONE");
	}
}
