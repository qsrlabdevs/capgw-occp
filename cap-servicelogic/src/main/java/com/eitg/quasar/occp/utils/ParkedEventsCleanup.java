package com.eitg.quasar.occp.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.Map;

public class ParkedEventsCleanup extends Thread {
    private static final Logger _log = LoggerFactory.getLogger(RedisProcessorThread.class.getName());
    private Map<String,Object> parkedEvents = null;

    public ParkedEventsCleanup(){
        this.parkedEvents = RedisProcessorThread.parkedEvents;
    }

    @Override
    public void run(){
        while(true){
            try {
//                long now = Instant.now().toEpochMilli();
//                parkedEvents.entrySet().removeIf(item -> (
//                        (now-item.getValue().timestamp)>20000)
//                );
                Thread.sleep(1000);
            } catch (Exception e){
                _log.error("Exception",e);
            }
        }
    }
}
