package com.eitg.quasar.occp.utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PendingEvent {
    public List<Map<String,Object>> existingEvents;
    public long timestamp=0;
    public PendingEvent(){
        timestamp= Instant.now().toEpochMilli();
        existingEvents = new ArrayList<>();
    }
}
