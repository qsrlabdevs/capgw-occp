package com.eitg.quasar.occp.utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import ch.qos.logback.core.joran.conditional.ThenAction;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.SlidingTimeWindowReservoir;
import com.eitg.quasar.occp.cap.metrics.MetricsServer;
import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eitg.quasar.occp.cap.businesslogic.JsonSession;
import com.eitg.quasar.occp.cap.utils.TelecomUtils;
import com.eitg.quasar.occp.cap.utils.TimerInformation.TIMER_TYPE;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.timer.Timer;
import redis.clients.jedis.exceptions.JedisException;

/**
 *
 * This class is used to fetch message from redis queue and issue specific commands
 *
 */
public class RedisProcessorThread extends Thread {
	static class RxThreadFactory implements ThreadFactory {
		public Thread newThread(Runnable r) {
			return new Thread(r, "RxPool");
		}
	}
	static class ArrayBlockingQueueWait<T> extends ArrayBlockingQueue<T> {

		ArrayBlockingQueueWait(int size, boolean fair) {
			super(size,fair);
		}

		public boolean offer(T task) {
			try {
				this.put(task);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			return true;
		}
	}
	public static ExecutorService rxPool=null;
	public static void initializeRxPool(int nbThreads){
		rxPool =new  ThreadPoolExecutor(nbThreads, nbThreads,
				0L, TimeUnit.MILLISECONDS,
				new ArrayBlockingQueueWait<>(nbThreads*5,true ),
				new RxThreadFactory());
	}

	private static final Logger _log = LoggerFactory.getLogger(RedisProcessorThread.class.getName());
	public static Map<String, Object> parkedEvents = new ConcurrentHashMap<>();
	private static SlidingTimeWindowReservoir slidingTimeWindowReservoir = new SlidingTimeWindowReservoir(1000, TimeUnit.MILLISECONDS);
	private static Histogram executionTimer = new Histogram(slidingTimeWindowReservoir);

	public static Histogram getExecutiontimer(){
		return executionTimer;
	}


	public static void parkEvent(String callId, Map<String, Object> event){
//		List<Map<String, Object>> existingEvents = (List<Map<String, Object>> ) parkedEvents.get(callId);
//		if( existingEvents==null){
//			existingEvents = new ArrayList<>();
//			parkedEvents.put(callId, existingEvents);
//		}

		List<Map<String, Object>> existingEvents = (List<Map<String, Object>> ) parkedEvents.computeIfAbsent(callId, k -> new ArrayList<>());
//        if( existingEvents==null){
//            existingEvents = new ArrayList<>();
//            parkedEvents.put(callId, existingEvents);
//        }

		existingEvents.add(event);
	}

	private String ingressQueue;

	public RedisProcessorThread(String ingressQueue){
		_log.debug("Instantiate new processor thread:{}",ingressQueue);
		this.ingressQueue=ingressQueue;
		synchronized (this.ingressQueue){
			if( cleanupPendingEvents==null ){
				cleanupPendingEvents = new CleanupPendingEvents();
				cleanupPendingEvents.start();
			}
		}
	}

	@Override
	public void run(){
		while(true){
			try {
				Map<String, Object> rawResponse = RedisPool.get().getFromQueue(ingressQueue);
				if (RedisPool.get() == null ||
						RedisPool.get().getDB() == null ||
						RedisPool.get().getDB().isClosed()) {
					_log.error("Redis server is not available, stop");
					Thread.sleep(1000);
					return;
				}
				_log.debug("GOT JSON:{}", rawResponse);
				if (rawResponse != null) {
					//fetch callid
					String callId = (String) rawResponse.get(JsonSCIFRequest.CallID);
					if (callId == null) {
						continue;
					}
					//
					Integer eventId = (Integer) rawResponse.get("id");
					String eventName = (String) rawResponse.get("eventname");
					String eventLockId = (String) rawResponse.get("event-id");


					String counter = "tas.cap.resp." + eventName;
					MetricsServer.get().counterIncrement(counter);

					//get execution time
					Long now = Instant.now().toEpochMilli();
					Long eventTime = (Long) rawResponse.get("timestamp");
					CapCallUser callUser = CapCallUser.calls.get(callId);
					//do not count vssf events
					if (eventTime != null ) {
						long diff = now - eventTime;
						counter = "tas.cap.time." + eventName;
						MetricsServer.get().setHistogram(counter, diff);


						if( !eventName.equalsIgnoreCase("vssf") ) {
							if( callUser!=null && callUser.isStateInitial()) {
								_log.info("Update execution timer session: {} diff: {}",callId,diff);
								executionTimer.update(diff);
							}
						}
						//log.error
						_log.info("Execution time session: {} value: {} mean: {} 95: {} 75: {} size:{}", callId, diff,
								executionTimer.getSnapshot().getMean(),
								executionTimer.getSnapshot().get95thPercentile(),
								executionTimer.getSnapshot().get75thPercentile(),
								executionTimer.getSnapshot().size());

						_log.debug("Execution time session: {} time: {}", callId, diff);
					}


					if( callUser!=null && !eventName.equalsIgnoreCase("makeCall") ){
						//enable overload for initialdp responses only
						callUser.firstResponseReceived() ;
					}

					Object parkedEvent = parkedEvents.get(callId);
					_log.debug("Parked event:{}", parkedEvent);

					if (eventName.equalsIgnoreCase("makeCall")) {
						List<Map<String, Object>> events = (List<Map<String, Object>>) parkedEvent;
						_log.debug("Running make call for:{}", rawResponse);
						Map<String, Object> lastEvent = null;
						if (events != null && events.size() > 0) {
							lastEvent = events.get(events.size() - 1);
						}


						Call call = null;
						JsonSession session = null;
						if (lastEvent != null) {
							call = (Call) lastEvent.get(JsonSCIFResponse.CALL);
							session = (JsonSession) lastEvent.get(JsonSCIFResponse.SESSION);
						}

						_log.debug("Last parked event:{} call:{} session:{}", lastEvent, call, session);

						JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, lastEvent);
						if (call != null && session != null) {
							_log.debug("Running make call on behalf on existing call user and session.");

							JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(call, resp);
							Timer timer = TelecomUtils.createTimer(timerListener, 0, session.getApplicationSession(), TIMER_TYPE.ASYNCHRONOUS_CALL);
						} else {
							if (rxPool != null) {
								rxPool.submit(new Runnable() {
									@Override
									public void run() {
										resp.execute();
									}
								});
							} else {
								resp.execute();
							}
						}
						continue;
					}

					//fetch event from parked events,
					//search for event id
					boolean eventFound = false;
					if (parkedEvent != null) {
						List<Map<String, Object>> events = (List<Map<String, Object>>) parkedEvent;
						int idx = 0;
						boolean found = false;
						for (Map<String, Object> event : events) {
							if (((Integer) event.get("id")) == eventId || eventName.equalsIgnoreCase("vssf")) {
								//found event
								_log.debug("Found event:{}", event);
								JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, event);
								Call call = (Call) event.get(JsonSCIFResponse.CALL);
								JsonSession session = (JsonSession) event.get(JsonSCIFResponse.SESSION);

								//create fake timer which fires imediately
								boolean async = false;
								if (session != null && session.getApplicationSession() != null) {
									JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(call, resp);
									_log.debug("Arming timer ...");
									try {
										Timer timer = TelecomUtils.createTimer(timerListener, 0, session.getApplicationSession(), TIMER_TYPE.ASYNCHRONOUS_CALL);
										async = true;
									} catch (Exception e) {
										_log.error("Exception", e);
										async = false;
									}
								}
								//in case timer failed then execute it
								if (!async) {
									found = true;
									if (rxPool != null) {
										rxPool.submit(new Runnable() {
											@Override
											public void run() {
												_log.debug("Handle parked event.");
												resp.execute();
											}
										});
									} else {
										resp.execute();
									}
								}
								if (!eventName.equalsIgnoreCase("vssf")) {
									found = true;
								}
								//remove item from list
								break;
							}
							idx++;
						}
						if (found) {
							eventFound = found;
							events.remove(idx);
						}
					}

					if (!eventFound) {
						//try to fetch call user based on session id

						//callUser = CapCallUser.calls.get(callId);
						_log.debug("Trying to process event for callid:{} callUser:{}", callId, callUser);
						if (callUser != null) {
							Map<String, Object> eventData = new HashMap<>();
							eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLSTART);

							eventData.put(JsonSCIFResponse.CALL, callUser.getCall());
							eventData.put(JsonSCIFResponse.SESSION, callUser.getJsonSession());
							eventData.put(JsonSCIFResponse.CALL_USER, callUser);

							_log.debug("Processing event out of sync:{} {}", callId, eventData);
							JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, eventData);

							/*
							if( callUser.getAppSession()!=null ) {
								_log.debug("Use timer instead of executor. app session:{} call:{}",callUser.getAppSession(),callUser.getCall());
								JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(callUser.getCall(), resp);
								Timer timer = TelecomUtils.createTimer(timerListener, 0, callUser.getAppSession(), TIMER_TYPE.ASYNCHRONOUS_CALL);
							} else {
								*/
							if (rxPool != null) {
								rxPool.submit(new Runnable() {
									@Override
									public void run() {
										_log.debug("Handle NON parked event, based on calluser session");
										resp.execute();
									}
								});
							} else {
								resp.execute();
							}
						}


						continue;
					}
					//}
				}
			} catch (JedisException je){
				try {
					_log.error("Jedis exception, sleep for 100 msec");
					Thread.sleep(100);
				} catch (Exception ss){

				}
			} catch (Exception e){
				_log.error("Exception",e);
			}
		}
	}

	public static void remove(String sipCallId) {
		_log.debug("Remove event:{}, parkedEvents size:{}",sipCallId,parkedEvents.size());
		parkedEvents.remove(sipCallId);
	}

	private class CleanupPendingEvents extends  Thread {
		@Override
		public void run(){
			while(true){
				try {
					long sizeBef = parkedEvents.size();
					parkedEvents.entrySet().removeIf(item -> (!CapCallUser.calls.containsKey(item.getKey())));
					_log.info("Pending events removed:{}",sizeBef-parkedEvents.size());
					Thread.sleep(1000);
				} catch (Exception e){
					_log.error("Exception",e);
				}
			}
		}
	}
	private static CleanupPendingEvents cleanupPendingEvents ;
}
