package com.eitg.quasar.occp.test.json;

import com.eitg.quasar.occp.cap.utils.TelecomUtils;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class BcdTests {
    @Test
    public void testBCD(){
        String bcd="00003019";
        String str = TelecomUtils.bcdToString(bcd);

        System.out.println(str);

        Integer.parseInt("0.0");

        bcd="02222122";
        str = TelecomUtils.bcdToString(bcd);

        System.out.println(str);


        try {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
            cal.setTime(sdf.parse(str));// all done
            System.out.println("Calendar:"+cal);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
