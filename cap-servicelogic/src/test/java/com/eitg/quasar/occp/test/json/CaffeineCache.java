package com.eitg.quasar.occp.test.json;

import com.eitg.quasar.nexus.middleware.utils.Commons;
import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class CaffeineCache {
    private static final Logger log = LoggerFactory.getLogger(CaffeineCache.class.getName());

    @Test
    public void testCaffeineCache(){
        try {
            Cache<String, String> myCache = Caffeine.newBuilder()
                    .expireAfterWrite(1, TimeUnit.SECONDS)
                    .build();

            myCache.put("test", "test");

            System.out.println("Get item:" + myCache.getIfPresent("test"));
            Thread.sleep(1500);
            System.out.println("Get item:" + myCache.getIfPresent("test"));
        } catch (Exception e){

        }
    }

    @Test
    public void testArray(){
        Integer[] eventsSkip = {4,7,17};
        log.debug("Events skip:{}",eventsSkip.length);
        for(int i=0;i<eventsSkip.length;i++){
            log.debug("item:{}",eventsSkip[i]);
        }

    }
}
