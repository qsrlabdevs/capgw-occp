package com.eitg.quasar.occp.test.json;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import com.eitg.quasar.occp.cap.metrics.MetricsServer;
import com.eitg.quasar.occp.cap.metrics.ValueGauge;
import com.eitg.quasar.occp.cap.servicelogic.CapCallUser;
import org.junit.Test;

public class GaugeTest {

    private MetricRegistry.MetricSupplier<Gauge> metricSupplier = new MetricRegistry.MetricSupplier<Gauge>() {
        @Override
        public Gauge newMetric() {
            return new ValueGauge();
        }
    };

    @Test
    public void testGauge(){
        String counterSize = "tas.sessions.calls";
        ValueGauge gauge = (ValueGauge) MetricsServer.get().getMetrics().gauge(counterSize, metricSupplier);
        gauge.setValue(CapCallUser.calls.size());
    }

    @Test
    public void testRegEx(){
        String eventName="callEnd.DISCONNECTED";
        String eventFilter="(callAnswered.*|callStart.*)";

        System.out.println(eventName.matches(eventFilter));
    }

    @Test
    public void testSeparator(){
        String queueName="logqueuedb/log_events";
        int posSeparator = queueName.indexOf(47);
        System.out.println("Position:"+posSeparator);
    }
}
