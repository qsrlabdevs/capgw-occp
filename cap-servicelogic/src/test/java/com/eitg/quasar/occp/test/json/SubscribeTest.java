package com.eitg.quasar.occp.test.json;

import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class SubscribeTest {

    @Test
    public void testSubscribe(){
        Jedis jedis = new Jedis("localhost");
        JedisPubSub command = new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                System.out.println("onMessage channel:"+channel+" message:"+message);
            }
        };
        jedis.subscribe(command,"tasvoice");

    }

}
