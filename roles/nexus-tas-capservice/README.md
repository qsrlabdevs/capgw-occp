Role Name
=========

It installs TAS CamelService component, copies ear file on TAS host

Requirements
------------

 * 

Role Variables
--------------

* nexus_install_path - specify path where package is deployed /opt/quasar
 
Dependencies
------------

 * none

Example Playbook
----------------


    - hosts: servers
      roles:
         - role: nexus-tas-capservice

           

License
-------

Quasar Software Research (c) 2018


